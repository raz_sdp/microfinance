<?php
App::uses('AppController', 'Controller');
/**
 * Accounttypes Controller
 *
 * @property Accounttype $Accounttype
 * @property PaginatorComponent $Paginator
 */
class AccounttypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Accounttype->recursive = 0;
		$this->set('accounttypes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Accounttype->exists($id)) {
			throw new NotFoundException(__('Invalid accounttype'));
		}
		$options = array('conditions' => array('Accounttype.' . $this->Accounttype->primaryKey => $id));
		$this->set('accounttype', $this->Accounttype->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Accounttype->create();
			if ($this->Accounttype->save($this->request->data)) {
				$this->Flash->success(__('The accounttype has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The accounttype could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Accounttype->exists($id)) {
			throw new NotFoundException(__('Invalid accounttype'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Accounttype->save($this->request->data)) {
				$this->Flash->success(__('The accounttype has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The accounttype could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Accounttype.' . $this->Accounttype->primaryKey => $id));
			$this->request->data = $this->Accounttype->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Accounttype->exists($id)) {
			throw new NotFoundException(__('Invalid accounttype'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Accounttype->delete($id)) {
			$this->Flash->success(__('The accounttype has been deleted.'));
		} else {
			$this->Flash->error(__('The accounttype could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
