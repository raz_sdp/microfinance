<?php
App::uses('AppController', 'Controller');
/**
 * Members Controller
 *
 * @property Member $Member
 * @property PaginatorComponent $Paginator
 */
class MembersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($Accounttype_id = 1) {
//		$this->Member->recursive = 0;
//		$this->set('members', $this->Paginator->paginate());
		$this->loadModel('Accounttype');
		$Accounttype =  $this->Accounttype->find('list',['fields' => ['Accounttype.id','Accounttype.name']]);
		$query = array(
			'conditions' => array(
				'Member.Accounttype_id' => $Accounttype_id
			)
		);

		$members = $this->Member->find('all',$query);

		$this->set(compact('Accounttype','members','Accounttype_id'));
		//pr($members);die;
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Member->exists($id)) {
			throw new NotFoundException(__('Invalid member'));
		}
		$options = array('conditions' => array('Member.' . $this->Member->primaryKey => $id));
		$this->set('member', $this->Member->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
            $staff_ids = array_filter($this->request->data['Staff']['id']);
			if(!empty($staff_ids)){
				$this->request->data['Member']['staff_id'] = '#'.implode('#',$staff_ids).'#';
				unset($this->request->data['Staff']);

			}


			//pr($this->Member->Account->id);
			//pr($this->request->data);die;
			// $this->request->data['Member']['staff_id'] = '#1#2#';
			$this->request->data['Member']['account_id'] = $this->Member->Account->id;

			if(!empty($this->request->data['Member']['memberPhoto']['name'])) {
				$this->request->data['Member']['memberPhoto'] = $this->_upload($this->request->data['Member']['memberPhoto'], 'image/member_image');
			 } else {
				unset($this->request->data['Member']['memberPhoto']);
			 }

			if(!empty($this->request->data['Member']['cardPhoto']['name'])) {
				$this->request->data['Member']['cardPhoto'] = $this->_upload($this->request->data['Member']['cardPhoto'], 'image/card_image');
			 } else {
				unset($this->request->data['Member']['cardPhoto']);
			 }

			if(!empty($this->request->data['Member']['nomPhoto']['name'])) {
				$this->request->data['Member']['nomPhoto'] = $this->_upload($this->request->data['Member']['nomPhoto'], 'image/nomine_image');
			 } else {
				unset($this->request->data['Member']['nomPhoto']);
			 }

			if(!empty($this->request->data['Member']['nomSign']['name'])) {
				$this->request->data['Member']['nomSign'] = $this->_upload($this->request->data['Member']['nomSign'], 'image/nomine_signature');
			 } else {
				unset($this->request->data['Member']['nomSign']);
			 }
            $this->request->data['Member']['account_id'] = $this->request->data['Account']['account_id'];
            $this->request->data['Member']['accounttype_id'] = $this->request->data['Account']['accounttype_id'];
			$this->Member->create();
			if ($this->Member->save($this->request->data['Member'])) {
			

				$this->request->data['Account']['member_id'] = $this->Member->id;
                $this->Member->Account->create();
				$this->Member->Account->save($this->request->data['Account']);

				// pr($this->request->data); die;
				$this->Session->setFlash(__('The Members has been saved.'), 'default', array('class' => 'alert alert-success text-center msg-margin'));
				//$this->Flash->success(__('The Members has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Flash->error(__(''));
				$this->Session->setFlash(__('The member could not be saved. Please, try again..'), 'default', array('class' => 'alert alert-success text-center msg-margin'));
			}
		}
		$staffs = $this->Member->Staff->find('all', [
			'recursive'=>-1,
			'fields' => ['Staff.id','Staff.name', 'Staff.moCode', 'Staff.pdmeCode', 'Staff.dmeCode','Staff.phone'],
			'conditions' => [
				'OR' => [
					['Staff.moCode IS NOT NULL'],
					['Staff.pdmeCode IS NOT NULL'],
					['Staff.dmeCode IS NOT NULL'],
				]
			]
		]);


		//pr($staffs);die;
		$accounts = $this->Member->Account->find('list');

		$accounttypes = $this->Member->Accounttype->find('list', ['fields'=> ['Accounttype.id','Accounttype.name']]);
		//pr($accounttypes);die;
		$this->set(compact('staffs', 'accounts', 'accounttypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Member->exists($id)) {
			throw new NotFoundException(__('Invalid member'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Member->save($this->request->data)) {
				//$this->Flash->success(__('The member has been saved.'));
				$this->Session->setFlash(__('The Members has been saved.'), 'default', array('class' => 'alert alert-success text-center msg-margin'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->Flash->error(__('The member could not be saved. Please, try again.'));
				$this->Session->setFlash(__('The Members  could not be saved.'), 'default', array('class' => 'alert alert-success text-center msg-margin'));
			}
		} else {
			$options = array('conditions' => array('Member.' . $this->Member->primaryKey => $id));
			$this->request->data = $this->Member->find('first', $options);
		}
		$staffs = $this->Member->Staff->find('list');
		$accounts = $this->Member->Account->find('list');
		$accounttypes = $this->Member->Accounttype->find('list');
		$this->set(compact('staffs', 'accounts', 'accounttypes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->Member->exists($id)) {
			throw new NotFoundException(__('Invalid member'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Member->delete($id)) {
			$this->Session->setFlash(__('The Members has been deleted.'), 'default', array('class' => 'alert alert-success text-center msg-margin'));
		} else {
			$this->Session->setFlash(__('The member could not be deleted. Please, try again.'), 'default', array('class' => 'alert alert-success text-center msg-margin'));

		}
		return $this->redirect(array('action' => 'index'));
	}
}
