<?php
/**
 * Member Fixture
 */
class MemberFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'memberName' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'memberFatherName' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'memberMotherName' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'birthDate' => array('type' => 'date', 'null' => true, 'default' => null),
		'age' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nationalIDNo' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'occupation' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'mobileNo' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'email' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'preAddress' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'prePost' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'preDistrict' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'preThana' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'perAddress' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'perPost' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'perDistrict' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'perThana' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomName' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomFather' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomMother' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomPreAddress' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomPost' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomThana' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomDistrict' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'relation' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomBirthDate' => array('type' => 'date', 'null' => true, 'default' => null),
		'pertnerPercent' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomMobile' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'memberPhoto' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'cardPhoto' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomPhoto' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'nomSign' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'staff_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'account_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'accounttype_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'memberName' => 'Lorem ipsum dolor sit amet',
			'memberFatherName' => 'Lorem ipsum dolor sit amet',
			'memberMotherName' => 'Lorem ipsum dolor sit amet',
			'birthDate' => '2019-05-13',
			'age' => 'Lorem ipsum dolor sit amet',
			'nationalIDNo' => 'Lorem ipsum dolor sit amet',
			'occupation' => 'Lorem ipsum dolor sit amet',
			'mobileNo' => 'Lorem ipsum dolor sit amet',
			'email' => 'Lorem ipsum dolor sit amet',
			'preAddress' => 'Lorem ipsum dolor sit amet',
			'prePost' => 'Lorem ipsum dolor sit amet',
			'preDistrict' => 'Lorem ipsum dolor sit amet',
			'preThana' => 'Lorem ipsum dolor sit amet',
			'perAddress' => 'Lorem ipsum dolor sit amet',
			'perPost' => 'Lorem ipsum dolor sit amet',
			'perDistrict' => 'Lorem ipsum dolor sit amet',
			'perThana' => 'Lorem ipsum dolor sit amet',
			'nomName' => 'Lorem ipsum dolor sit amet',
			'nomFather' => 'Lorem ipsum dolor sit amet',
			'nomMother' => 'Lorem ipsum dolor sit amet',
			'nomPreAddress' => 'Lorem ipsum dolor sit amet',
			'nomPost' => 'Lorem ipsum dolor sit amet',
			'nomThana' => 'Lorem ipsum dolor sit amet',
			'nomDistrict' => 'Lorem ipsum dolor sit amet',
			'relation' => 'Lorem ipsum dolor sit amet',
			'nomBirthDate' => '2019-05-13',
			'pertnerPercent' => 'Lorem ipsum dolor sit amet',
			'nomMobile' => 'Lorem ipsum dolor sit amet',
			'memberPhoto' => 'Lorem ipsum dolor sit amet',
			'cardPhoto' => 'Lorem ipsum dolor sit amet',
			'nomPhoto' => 'Lorem ipsum dolor sit amet',
			'nomSign' => 'Lorem ipsum dolor sit amet',
			'staff_id' => 1,
			'account_id' => 1,
			'accounttype_id' => 1,
			'modified' => '2019-05-13 11:01:22',
			'created' => '2019-05-13 11:01:22'
		),
	);

}
