<?php
/**
 * Account Fixture
 */
class AccountFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'member_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'accounttype_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'accNo' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'accCreatedAt' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'period' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'storageAmmount' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'totlaAmmount' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'rcvProfit' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'depositAmmount' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'expiredDate' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'admissionFee' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'withdraw' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'member_id' => 1,
			'accounttype_id' => 1,
			'modified' => '2019-05-13 10:38:58',
			'accNo' => 'Lorem ipsum dolor sit amet',
			'accCreatedAt' => '2019-05-13 10:38:58',
			'period' => 'Lorem ipsum dolor sit amet',
			'storageAmmount' => 'Lorem ipsum dolor sit amet',
			'totlaAmmount' => 'Lorem ipsum dolor sit amet',
			'rcvProfit' => 'Lorem ipsum dolor sit amet',
			'depositAmmount' => 'Lorem ipsum dolor sit amet',
			'expiredDate' => '2019-05-13 10:38:58',
			'admissionFee' => 'Lorem ipsum dolor sit amet',
			'withdraw' => 'Lorem ipsum dolor sit amet',
			'created' => '2019-05-13 10:38:58'
		),
	);

}
