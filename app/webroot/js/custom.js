$(function(){
	// Add Designation
	$('.js-add-designation').on('click', function(){
		var designation = $('.js-designation-inp').val();
		$.post(ROOT+'admin/designations/add', {"data[Designation][designation]" : designation}, function(res){
			if(res.success){
				$('.js-desig-group').append('<li class="list-group-item list-group-item-success">'+designation+'</li>');
			} else {

			}
			console.log(res);return false;
		},'json');
	});

	// Add Group for Staff
	$('.js-add-group').on('click', function(){
		var grpCode = $('.js-grpCode-inp').val();
		var grpName = $('.js-grpName-inp').val();
		var president = $('.js-president-inp').val();
		var village = $('.js-village-inp').val();
		$.post(ROOT+'admin/groups/add', {"data[Group][grpCode]" : grpCode ,"data[Group][grpName]" : grpName ,"data[Group][president]" : president ,"data[Group][village]" : village }, function(res){
			if(res.success){
				$('.js-group-list').append('<tr class="text-success"><td>'+ grpCode +'</td><td>'+ grpName +'</td><td>'+ president +'</td><td>'+ village +'</td></tr>');
			} else {
				console.log("Error");
			}
			console.log(res);return false;
		},'json');
	});

    //Manage MO code
    $('.js-mo-code').on('change',function(){
        select('.js-mo-code','.name-js','.mobile-js','.mo');
    })

    $('.js-pdme-code').on('change',function(){
        select('.js-pdme-code','.aname-js','.amobile-js','.pdme');
    })


    $('.js-dme-code').on('change',function(){
        select('.js-dme-code','.bname-js','.bmobile-js','.dme');
    })


    function select(first,second,third,forth){
        var data = $(first).val();
        var id = $(first).next().find("[value='" + data + "']");
        $(forth).val(id.data('id'));
        console.log(id.data('id'))
        var result = data.split('~');
        $(first).val(result[1].trim());
        $(second).val(result[0].trim());
        $(third).val(result[2].trim());


    }


	//form validation




});

