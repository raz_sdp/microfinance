<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));	
	Router::connect('/dashboard', array('controller' => 'users', 'action' => 'dashboard','admin' => true));
	Router::connect('/member-list', array('controller' => 'pages', 'action' => 'display', 'member-list'));
	Router::connect('/ds-members', array('controller' => 'pages', 'action' => 'display', 'ds-members'));
	Router::connect('/loan-members', array('controller' => 'pages', 'action' => 'display', 'loan-members'));
	Router::connect('/ms-members', array('controller' => 'pages', 'action' => 'display', 'ms-members'));
	Router::connect('/ps-members', array('controller' => 'pages', 'action' => 'display', 'ps-members'));
	Router::connect('/psp-members', array('controller' => 'pages', 'action' => 'display', 'psp-members'));
	Router::connect('/ys-members', array('controller' => 'pages', 'action' => 'display', 'ys-members'));
	Router::connect('/saving-acc-form', array('controller' => 'pages', 'action' => 'display', 'saving-acc-form'));
	Router::connect('/saving-collect', array('controller' => 'pages', 'action' => 'display', 'saving-collect'));
	Router::connect('/gs-transaction', array('controller' => 'pages', 'action' => 'display', 'gs-transaction'));
	Router::connect('/ds-transaction', array('controller' => 'pages', 'action' => 'display', 'ds-transaction'));
	Router::connect('/saving-transaction', array('controller' => 'pages', 'action' => 'display', 'saving-transaction'));
	Router::connect('/ys-transaction', array('controller' => 'pages', 'action' => 'display', 'ys-transaction'));
	Router::connect('/diposit_withdrawal', array('controller' => 'pages', 'action' => 'display', 'diposit_withdrawal'));
	Router::connect('/saving_acc_closing', array('controller' => 'pages', 'action' => 'display', 'saving_acc_closing'));
	Router::connect('/loan_acc_open', array('controller' => 'pages', 'action' => 'display', 'loan_acc_open'));
	Router::connect('/loan-acc-close', array('controller' => 'pages', 'action' => 'display', 'loan-acc-close'));
	Router::connect('/loan-collect', array('controller' => 'pages', 'action' => 'display', 'loan-collect'));
	Router::connect('/staff', array('controller' => 'pages', 'action' => 'display', 'staff'));
	Router::connect('/staff-acc-open', array('controller' => 'pages', 'action' => 'display', 'staff-acc-open'));
	Router::connect('/income', array('controller' => 'pages', 'action' => 'display', 'income'));
	Router::connect('/payment', array('controller' => 'pages', 'action' => 'display', 'payment'));
	Router::connect('/transfer', array('controller' => 'pages', 'action' => 'display', 'transfer'));
	Router::connect('/adjustment', array('controller' => 'pages', 'action' => 'display', 'adjustment'));
	Router::connect('/bank-reconciliation', array('controller' => 'pages', 'action' => 'display', 'bank-reconciliation'));
	Router::connect('/acc-head', array('controller' => 'pages', 'action' => 'display', 'acc-head'));
	Router::connect('/ledger-chart', array('controller' => 'pages', 'action' => 'display', 'ledger-chart'));
	Router::connect('/bank-acc-open', array('controller' => 'pages', 'action' => 'display', 'bank-acc-open'));
	Router::connect('/acc-chart', array('controller' => 'pages', 'action' => 'display', 'acc-chart'));
	Router::connect('/profit-payment', array('controller' => 'pages', 'action' => 'display', 'profit-payment'));
	Router::connect('/reports', array('controller' => 'pages', 'action' => 'display', 'reports'));
	Router::connect('/settings', array('controller' => 'pages', 'action' => 'display', 'settings'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
