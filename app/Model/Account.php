<?php
App::uses('AppModel', 'Model');
/**
 * Account Model
 *
 * @property Member $Member
 * @property Accounttype $Accounttype
 */
class Account extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Member' => array(
			'className' => 'Member',
			'foreignKey' => 'member_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Accounttype' => array(
			'className' => 'Accounttype',
			'foreignKey' => 'accounttype_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
