<?php
App::uses('AppModel', 'Model');
/**
 * Designation Model
 *
 * @property Staff $Staff
 */
class Designation extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'designation_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
