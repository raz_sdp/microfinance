<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $this->fetch('title'); ?>
    </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <?php
    echo $this->Html->meta('icon');

    echo $this->Html->css([
        'plugins/font-awesome/css/font-awesome.min',
        'dist/css/adminlte.min',
        'plugins/iCheck/flat/blue',
        'plugins/morris/morris',
        'plugins/jvectormap/jquery-jvectormap-1.2.2',
        'plugins/datepicker/datepicker3',
        'plugins/daterangepicker/daterangepicker-bs3',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min',
    ]);
    echo $this->Html->script([
        'plugins/bootstrap/js/bootstrap.bundle.min',
        'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
        'plugins/morris/morris.min',
        'plugins/sparkline/jquery.sparkline.min',
        'plugins/jvectormap/jquery-jvectormap-1.2.2.min',
        'plugins/jvectormap/jquery-jvectormap-world-mill-en',
        'plugins/knob/jquery.knob',
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js',
        'plugins/daterangepicker/daterangepicker',
        'plugins/daterangepicker/daterangepicker',
        'plugins/datepicker/bootstrap-datepicker',
        'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all',
        'plugins/slimScroll/jquery.slimscroll.min',
        'plugins/fastclick/fastclick',
        'dist/js/adminlte',
        'dist/js/pages/dashboard',
        'dist/js/demo',
        'custom',

    ]);

    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
    
    <script>
        var ROOT = '<?php echo $this->Html->url('/', true); ?>';
        var HERE = '<?php echo $this->here; ?>';
    </script>

</head>
<body class="hold-transition login-page">


<div id="content">

    <?php echo $this->Session->flash(); ?>

    <?php echo $this->fetch('content'); ?>

</div>
<!--Footer-->
    <?php //echo $this->element('admin-footer'); ?>
<!--/Footer-pt>
</body>
</html>
