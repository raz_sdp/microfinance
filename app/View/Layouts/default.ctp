<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'সমবায় সমিতি লিঃ');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?> |
		<?php echo $this->fetch('title'); ?>
	</title>
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	
	<?php
	echo $this->Html->meta('icon');

	echo $this->Html->css([
		//'cake.generic',
		'plugins/font-awesome/css/font-awesome.min',
		'dist/css/adminlte.min',
		'plugins/iCheck/flat/blue',
		'plugins/morris/morris',
		'plugins/jvectormap/jquery-jvectormap-1.2.2',
		'plugins/datepicker/datepicker3',
		'plugins/daterangepicker/daterangepicker-bs3',
		'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min',
	
	]);
	
	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>

	<script>
        var ROOT = '<?php echo $this->Html->url('/', true); ?>';
        var HERE = '<?php echo $this->here; ?>';
    </script>
</head>
<body class="hold-transition sidebar-mini">
	<div class="wrapper">
        <?php
       // pr($this->params['controller']);die;
        if(!($this->params['controller']=='users' && $this->params['action']=='admin_login' )){
            ?>
		<?php echo $this->element('admin-header');?>	
			<?php echo $this->element('admin-left-column');?>
        <?php } ?>
		<div class="content-wrapper">			
			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<?php echo $this->element('admin-footer'); ?>
		
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
