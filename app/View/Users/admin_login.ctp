<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <h4>বটিয়াঘাটা সার্বিক গ্রাম উন্নয়ন সমবায় সমিতি লিঃ</h4>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

        <?php echo $this->Form->create('User'); ?>
        <div class="form-group has-feedback">
            <label class="sr-only" for="inlineFormInputGroupUsername">Email</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><span class="fa fa-envelope form-control-feedback"></span></div>
                </div>
                <input type="text" name="data[User][email]" class="form-control" id="inlineFormInputGroupUsername" placeholder="Username">
            </div>
        </div>
        <div class="form-group has-feedback">
            <label class="sr-only" for="inlineFormInputGroupUsername">Password</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><span class="fa fa-lock form-control-feedback"></span></div>
                </div>
                <input type="password" name="data[User][password]" class="form-control" id="inlineFormInputGroupUsername" placeholder="Password">
            </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-4 offset-4">
            <button type="submit" class="btn btn-block btn-outline-primary">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->


</body>