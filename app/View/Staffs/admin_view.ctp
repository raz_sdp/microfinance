<div class="staffs view">
<h2><?php echo __('Staff'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['address']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['phone']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NationalIDNo'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['nationalIDNo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BloodGrp'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['bloodGrp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Father'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['father']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mother'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['mother']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Salary'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['salary']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('JoiningDate'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['joiningDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Group'); ?></dt>
		<dd>
			<?php echo $this->Html->link($staff['Group']['id'], array('controller' => 'groups', 'action' => 'view', $staff['Group']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Designation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($staff['Designation']['id'], array('controller' => 'designations', 'action' => 'view', $staff['Designation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($staff['Staff']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Staff'), array('action' => 'edit', $staff['Staff']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Staff'), array('action' => 'delete', $staff['Staff']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $staff['Staff']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Designations'), array('controller' => 'designations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Designation'), array('controller' => 'designations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Members'), array('controller' => 'members', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member'), array('controller' => 'members', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Members'); ?></h3>
	<?php if (!empty($staff['Member'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('MemberName'); ?></th>
		<th><?php echo __('MemberFatherName'); ?></th>
		<th><?php echo __('MemberMotherName'); ?></th>
		<th><?php echo __('BirthDate'); ?></th>
		<th><?php echo __('Age'); ?></th>
		<th><?php echo __('NationalIDNo'); ?></th>
		<th><?php echo __('Occupation'); ?></th>
		<th><?php echo __('MobileNo'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('PreAddress'); ?></th>
		<th><?php echo __('PrePost'); ?></th>
		<th><?php echo __('PreDistrict'); ?></th>
		<th><?php echo __('PreThana'); ?></th>
		<th><?php echo __('PerAddress'); ?></th>
		<th><?php echo __('PerPost'); ?></th>
		<th><?php echo __('PerDistrict'); ?></th>
		<th><?php echo __('PerThana'); ?></th>
		<th><?php echo __('NomName'); ?></th>
		<th><?php echo __('NomFather'); ?></th>
		<th><?php echo __('NomMother'); ?></th>
		<th><?php echo __('NomPreAddress'); ?></th>
		<th><?php echo __('NomPost'); ?></th>
		<th><?php echo __('NomThana'); ?></th>
		<th><?php echo __('NomDistrict'); ?></th>
		<th><?php echo __('Relation'); ?></th>
		<th><?php echo __('NomBirthDate'); ?></th>
		<th><?php echo __('PertnerPercent'); ?></th>
		<th><?php echo __('NomMobile'); ?></th>
		<th><?php echo __('MemberPhoto'); ?></th>
		<th><?php echo __('CardPhoto'); ?></th>
		<th><?php echo __('NomPhoto'); ?></th>
		<th><?php echo __('NomSign'); ?></th>
		<th><?php echo __('Staff Id'); ?></th>
		<th><?php echo __('Accounts Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Accountstypes Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($staff['Member'] as $member): ?>
		<tr>
			<td><?php echo $member['id']; ?></td>
			<td><?php echo $member['memberName']; ?></td>
			<td><?php echo $member['memberFatherName']; ?></td>
			<td><?php echo $member['memberMotherName']; ?></td>
			<td><?php echo $member['birthDate']; ?></td>
			<td><?php echo $member['age']; ?></td>
			<td><?php echo $member['nationalIDNo']; ?></td>
			<td><?php echo $member['occupation']; ?></td>
			<td><?php echo $member['mobileNo']; ?></td>
			<td><?php echo $member['email']; ?></td>
			<td><?php echo $member['preAddress']; ?></td>
			<td><?php echo $member['prePost']; ?></td>
			<td><?php echo $member['preDistrict']; ?></td>
			<td><?php echo $member['preThana']; ?></td>
			<td><?php echo $member['perAddress']; ?></td>
			<td><?php echo $member['perPost']; ?></td>
			<td><?php echo $member['perDistrict']; ?></td>
			<td><?php echo $member['perThana']; ?></td>
			<td><?php echo $member['nomName']; ?></td>
			<td><?php echo $member['nomFather']; ?></td>
			<td><?php echo $member['nomMother']; ?></td>
			<td><?php echo $member['nomPreAddress']; ?></td>
			<td><?php echo $member['nomPost']; ?></td>
			<td><?php echo $member['nomThana']; ?></td>
			<td><?php echo $member['nomDistrict']; ?></td>
			<td><?php echo $member['relation']; ?></td>
			<td><?php echo $member['nomBirthDate']; ?></td>
			<td><?php echo $member['pertnerPercent']; ?></td>
			<td><?php echo $member['nomMobile']; ?></td>
			<td><?php echo $member['memberPhoto']; ?></td>
			<td><?php echo $member['cardPhoto']; ?></td>
			<td><?php echo $member['nomPhoto']; ?></td>
			<td><?php echo $member['nomSign']; ?></td>
			<td><?php echo $member['staff_id']; ?></td>
			<td><?php echo $member['accounts_id']; ?></td>
			<td><?php echo $member['modified']; ?></td>
			<td><?php echo $member['accountstypes_id']; ?></td>
			<td><?php echo $member['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'members', 'action' => 'view', $member['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'members', 'action' => 'edit', $member['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'members', 'action' => 'delete', $member['id']), array('confirm' => __('Are you sure you want to delete # %s?', $member['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Member'), array('controller' => 'members', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
