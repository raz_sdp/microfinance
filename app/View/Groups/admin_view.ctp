<div class="groups view">
<h2><?php echo __('Group'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($group['Group']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('GrpCode'); ?></dt>
		<dd>
			<?php echo h($group['Group']['grpCode']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('GrpName'); ?></dt>
		<dd>
			<?php echo h($group['Group']['grpName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('President'); ?></dt>
		<dd>
			<?php echo h($group['Group']['president']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Village'); ?></dt>
		<dd>
			<?php echo h($group['Group']['village']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($group['Group']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mmodified'); ?></dt>
		<dd>
			<?php echo h($group['Group']['mmodified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Group'), array('action' => 'edit', $group['Group']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Group'), array('action' => 'delete', $group['Group']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $group['Group']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Staffs'); ?></h3>
	<?php if (!empty($group['Staff'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('NationalIDNo'); ?></th>
		<th><?php echo __('BloodGrp'); ?></th>
		<th><?php echo __('Father'); ?></th>
		<th><?php echo __('Mother'); ?></th>
		<th><?php echo __('Salary'); ?></th>
		<th><?php echo __('JoiningDate'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('Designation Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($group['Staff'] as $staff): ?>
		<tr>
			<td><?php echo $staff['id']; ?></td>
			<td><?php echo $staff['code']; ?></td>
			<td><?php echo $staff['name']; ?></td>
			<td><?php echo $staff['address']; ?></td>
			<td><?php echo $staff['phone']; ?></td>
			<td><?php echo $staff['email']; ?></td>
			<td><?php echo $staff['nationalIDNo']; ?></td>
			<td><?php echo $staff['bloodGrp']; ?></td>
			<td><?php echo $staff['father']; ?></td>
			<td><?php echo $staff['mother']; ?></td>
			<td><?php echo $staff['salary']; ?></td>
			<td><?php echo $staff['joiningDate']; ?></td>
			<td><?php echo $staff['group_id']; ?></td>
			<td><?php echo $staff['designation_id']; ?></td>
			<td><?php echo $staff['created']; ?></td>
			<td><?php echo $staff['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'staffs', 'action' => 'view', $staff['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'staffs', 'action' => 'edit', $staff['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'staffs', 'action' => 'delete', $staff['id']), array('confirm' => __('Are you sure you want to delete # %s?', $staff['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
