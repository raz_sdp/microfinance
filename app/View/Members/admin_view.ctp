<div class="members view">
<h2><?php echo __('Member'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($member['Member']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MemberName'); ?></dt>
		<dd>
			<?php echo h($member['Member']['memberName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MemberFatherName'); ?></dt>
		<dd>
			<?php echo h($member['Member']['memberFatherName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MemberMotherName'); ?></dt>
		<dd>
			<?php echo h($member['Member']['memberMotherName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('BirthDate'); ?></dt>
		<dd>
			<?php echo h($member['Member']['birthDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Age'); ?></dt>
		<dd>
			<?php echo h($member['Member']['age']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NationalIDNo'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nationalIDNo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Occupation'); ?></dt>
		<dd>
			<?php echo h($member['Member']['occupation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MobileNo'); ?></dt>
		<dd>
			<?php echo h($member['Member']['mobileNo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($member['Member']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PreAddress'); ?></dt>
		<dd>
			<?php echo h($member['Member']['preAddress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PrePost'); ?></dt>
		<dd>
			<?php echo h($member['Member']['prePost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PreDistrict'); ?></dt>
		<dd>
			<?php echo h($member['Member']['preDistrict']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PreThana'); ?></dt>
		<dd>
			<?php echo h($member['Member']['preThana']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PerAddress'); ?></dt>
		<dd>
			<?php echo h($member['Member']['perAddress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PerPost'); ?></dt>
		<dd>
			<?php echo h($member['Member']['perPost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PerDistrict'); ?></dt>
		<dd>
			<?php echo h($member['Member']['perDistrict']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PerThana'); ?></dt>
		<dd>
			<?php echo h($member['Member']['perThana']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomName'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomName']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomFather'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomFather']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomMother'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomMother']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomPreAddress'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomPreAddress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomPost'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomPost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomThana'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomThana']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomDistrict'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomDistrict']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Relation'); ?></dt>
		<dd>
			<?php echo h($member['Member']['relation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomBirthDate'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomBirthDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('PertnerPercent'); ?></dt>
		<dd>
			<?php echo h($member['Member']['pertnerPercent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomMobile'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomMobile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('MemberPhoto'); ?></dt>
		<dd>
			<?php echo h($member['Member']['memberPhoto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('CardPhoto'); ?></dt>
		<dd>
			<?php echo h($member['Member']['cardPhoto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomPhoto'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomPhoto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('NomSign'); ?></dt>
		<dd>
			<?php echo h($member['Member']['nomSign']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Staff'); ?></dt>
		<dd>
			<?php echo $this->Html->link($member['Staff']['name'], array('controller' => 'staffs', 'action' => 'view', $member['Staff']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Account'); ?></dt>
		<dd>
			<?php echo $this->Html->link($member['Account']['id'], array('controller' => 'accounts', 'action' => 'view', $member['Account']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accounttype'); ?></dt>
		<dd>
			<?php echo $this->Html->link($member['Accounttype']['id'], array('controller' => 'accounttypes', 'action' => 'view', $member['Accounttype']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($member['Member']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($member['Member']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Member'), array('action' => 'edit', $member['Member']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Member'), array('action' => 'delete', $member['Member']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $member['Member']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Members'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounts'), array('controller' => 'accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('controller' => 'accounttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('controller' => 'accounttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Accounts'); ?></h3>
	<?php if (!empty($member['Account'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Member Id'); ?></th>
		<th><?php echo __('Accounttype Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('AccNo'); ?></th>
		<th><?php echo __('AccCreatedAt'); ?></th>
		<th><?php echo __('Period'); ?></th>
		<th><?php echo __('StorageAmmount'); ?></th>
		<th><?php echo __('TotlaAmmount'); ?></th>
		<th><?php echo __('RcvProfit'); ?></th>
		<th><?php echo __('DepositAmmount'); ?></th>
		<th><?php echo __('ExpiredDate'); ?></th>
		<th><?php echo __('AdmissionFee'); ?></th>
		<th><?php echo __('Withdraw'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($member['Account'] as $account): ?>
		<tr>
			<td><?php echo $account['id']; ?></td>
			<td><?php echo $account['member_id']; ?></td>
			<td><?php echo $account['accounttype_id']; ?></td>
			<td><?php echo $account['modified']; ?></td>
			<td><?php echo $account['accNo']; ?></td>
			<td><?php echo $account['accCreatedAt']; ?></td>
			<td><?php echo $account['period']; ?></td>
			<td><?php echo $account['storageAmmount']; ?></td>
			<td><?php echo $account['totlaAmmount']; ?></td>
			<td><?php echo $account['rcvProfit']; ?></td>
			<td><?php echo $account['depositAmmount']; ?></td>
			<td><?php echo $account['expiredDate']; ?></td>
			<td><?php echo $account['admissionFee']; ?></td>
			<td><?php echo $account['withdraw']; ?></td>
			<td><?php echo $account['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'accounts', 'action' => 'view', $account['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'accounts', 'action' => 'edit', $account['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accounts', 'action' => 'delete', $account['id']), array('confirm' => __('Are you sure you want to delete # %s?', $account['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
