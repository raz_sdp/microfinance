<div class="members form">
<?php echo $this->Form->create('Member'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Member'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('memberName');
		echo $this->Form->input('memberFatherName');
		echo $this->Form->input('memberMotherName');
		echo $this->Form->input('birthDate');
		echo $this->Form->input('age');
		echo $this->Form->input('nationalIDNo');
		echo $this->Form->input('occupation');
		echo $this->Form->input('mobileNo');
		echo $this->Form->input('email');
		echo $this->Form->input('preAddress');
		echo $this->Form->input('prePost');
		echo $this->Form->input('preDistrict');
		echo $this->Form->input('preThana');
		echo $this->Form->input('perAddress');
		echo $this->Form->input('perPost');
		echo $this->Form->input('perDistrict');
		echo $this->Form->input('perThana');
		echo $this->Form->input('nomName');
		echo $this->Form->input('nomFather');
		echo $this->Form->input('nomMother');
		echo $this->Form->input('nomPreAddress');
		echo $this->Form->input('nomPost');
		echo $this->Form->input('nomThana');
		echo $this->Form->input('nomDistrict');
		echo $this->Form->input('relation');
		echo $this->Form->input('nomBirthDate');
		echo $this->Form->input('pertnerPercent');
		echo $this->Form->input('nomMobile');
		echo $this->Form->input('memberPhoto');
		echo $this->Form->input('cardPhoto');
		echo $this->Form->input('nomPhoto');
		echo $this->Form->input('nomSign');
		echo $this->Form->input('staff_id');
		echo $this->Form->input('account_id');
		echo $this->Form->input('accounttype_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Member.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Member.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Members'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounts'), array('controller' => 'accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('controller' => 'accounttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('controller' => 'accounttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
