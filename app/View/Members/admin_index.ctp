<?php
//pr($Accounttype);die;
$savings_type = $this->params['pass'][0];
//pr($savings_type);die;
?>
<div class="card-body">
	<div class="card card-default">
	  <div class="card-header bg-secondary p-0">
		<nav class="navbar navbar-expand-lg navbar-light">
	  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<?php foreach ($Accounttype as $key => $value) {?>
				  <li class="nav-item ">
					<a class="nav-link <?php echo $savings_type==$key ? 'bg-primary' : ''?>" href="<?php echo $this->Html->url('/admin/members/index/'.$key) ?>"><?php echo $value ?></a>
				  </li>
				<?php } ?>
				  
				</ul>
			</div>
		</nav>
	  
	</div>
		<div class="card-body">
			<div class="row">
				  <div class="col-12">
					<div class="card">
					  <div class="card-header bg-primary">
						<h3 class="card-title">সাধারণ সঞ্চয়ী সদস্যের তালিকা</h3>

						<div class="card-tools">
						  <div class="input-group input-group-sm" style="width: 150px;">
							<input type="text" name="table_search" class="form-control float-right" placeholder="Search">

							<div class="input-group-append">
							  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
							</div>
						  </div>
						</div>
					  </div>
					  <!-- /.card-header -->
					  <div class="card-body table-responsive p-0">
						<table class="table table-hover small">
						  <tr>
							<th>ক্রঃনং</th>
							<th>হিসাব নং</th>
							<th>নাম</th>
							<th>ঠিকানা</th>
							<th>উপজেলা</th>
							<th>মোবাইল নং.</th>
							<th>খোলার তাং</th>
							<?php 
								switch ($Accounttype_id) { 
									//সাধারণ সঞ্চয়ী হিসাব
									case '1':
								?>
									<th>মাসিক সঞ্চয়</th>
									<th>মোট ব্যালাস্ন </th>
								<?php 	break; 
									//দৈনিক সঞ্চয়ী হিসাব
									case '2':
								?>
									<th>মেয়াদ(বছর)</th>
									<th>দৈনিক সঞ্চয়</th>
									<th>মোট ব্যালাস্ন </th>
								<?php 	break;
									// বাৎসরিক সঞ্চয়ী হিসাব 
									case '3':
								?>
									<th>মেয়াদ(বছর)</th>
									<th>বাৎসরিক সঞ্চয়</th>
									<th>মোট ব্যালাস্ন </th>

								<?php 	break; 
								//পি এস পি
									case '4':
								?>
									<th>মেয়াদ(বছর)</th>
									<th>জমাকৃত</th>
									<th>মেয়াদপূর্তির পরিমাণ</th>
								<?php 	break; 
								//মাসিক সঞ্চয়ী হিসাব
									case '5':
								?>
									<th>মেয়াদ(বছর)</th>
									<th>মাসিক সঞ্চয়</th>
									<th>মোট ব্যালাস্ন </th>

								<?php 	break; 
								//পি এস
									case '6':
								?>
									<th>মেয়াদ(বছর)</th>
									<th>জমাকৃত	</th>
									<th>মাসিক লভাংশ্য</th>
								<?php 	break; 
								// লোন
									case '7':
								?>
									<th>মেয়াদ(বছর)</th>
									<th>দৈনিক সঞ্চয়</th>
									<th>মোট ব্যালাস্ন </th>
								<?php
									default:
										# code...
										break;
								}

							 ?>

							<th>উত্তোলন</th>
							<th>সঞ্চয় আদায়</th>
							<th>এডিট</th>
							<th>প্রিন্ট</th>
						  </tr>
						  <?php 

						   ?>
						   <?php foreach ($members as $member) {
						   	// pr($members);die;
						   	// print_r($member);die;
						    ?>
						  <tr>
							<td><?php echo h($member['Member']['id']); ?>&nbsp;</td>
							<td><?php echo h($member['Account']['0']['accNo']); ?>&nbsp;</td>
							<td><?php echo h($member['Member']['memberName']); ?>&nbsp;</td>
							<td><?php echo h($member['Member']['preAddress']); ?>&nbsp;</td>
							<td><?php echo h($member['Member']['preDistrict']); ?>&nbsp;</td>
							<td><?php echo h($member['Member']['mobileNo']); ?>&nbsp;</td>
							<td><?php echo h($member['Account']['0']['accCreatedAt']); ?>&nbsp;</td>
							<?php 
								switch ($Accounttype_id) { 
									//সাধারণ সঞ্চয়ী হিসাব
									case '1':
								?>
									<td><?php echo h($member['Account']['0']['storageAmmount']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['totlaAmmount']); ?>&nbsp;</td>
									
								<?php 	break; 
									//দৈনিক সঞ্চয়ী হিসাব
									case '2':
								?>
									<td><?php echo h($member['Account']['0']['period']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['storageAmmount']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['totlaAmmount']); ?>&nbsp;</td>

								<?php 	break;
									// বাৎসরিক সঞ্চয়ী হিসাব 
									case '3':
								?>
									<td><?php echo h($member['Account']['0']['period']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['storageAmmount']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['totlaAmmount']); ?>&nbsp;</td>


								<?php 	break; 
								//পি এস পি
									case '4':
								?>
									<td><?php echo h($member['Account']['0']['period']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['depositAmmount']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['rcvProfit']); ?>&nbsp;</td>
									<!-- <th>মেয়াদপূর্তির পরিমাণ</th> there is a problem -->
								<?php 	break; 
								//মাসিক সঞ্চয়ী হিসাব
									case '5':
								?>
									<td><?php echo h($member['Account']['0']['period']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['storageAmmount']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['totlaAmmount']); ?>&nbsp;</td>
								<?php 	break; 
								//পি এস
									case '6':
								?>
									<td><?php echo h($member['Account']['0']['period']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['depositAmmount']); ?>&nbsp;</td>
									<td><?php echo h($member['Account']['0']['rcvProfit']); ?>&nbsp;</td>
								<?php 	break; 
								// লোন
									case '7':
								?>
<!-- 								<th>মেয়াদ(বছর)</th>
									<th>দৈনিক সঞ্চয়</th>
									<th>মোট ব্যালাস্ন </th> -->
								<?php
									default:
										# code...
										break;
								}

							 ?>	
							
							<td><?php echo h($member['Account']['0']['withdraw']); ?>&nbsp;</td>
							<td></td>
							<td>Edit</td>
							<td>Print</td>
						  </tr>
						  <?php } ?>
						</table>
					  </div>
					  <!-- /.card-body -->
					</div>
					<!-- /.card -->
				  </div>
				</div>
		  </div>
	</div>
</div>