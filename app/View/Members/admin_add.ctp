<style>
	.valid-feedback.feedback-icon, .invalid-feedback.feedback-icon {
		position: absolute;
		width: auto;
		bottom: -41px;
		right: 25px;
		margin-top: 0;
		height: 100%;
	}
</style>
<div class="card-body">
<div class="card card-default">
<div class="card-header bg-secondary">
	<h3 class="card-title text-center">সঞ্চয়ী হিসাব খুলুন</h3>
</div>
<div class="card-body">
<!-- form start -->
<?php echo $this->Form->create('Member',
	array(
		'enctype' => 'multipart/form-data',
		'class' => 'needs-validation',
		'novalidate'
		)); ?>

<div class="card-body">
<div class="card card-default"> <!-- card1 -->
	<div class="card-header bg-primary">
		<h3 class="card-title">গ্রাহকের তথ্য</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row">
			<div class="form-group col-md-4">
				<!-- <label for=""></label>
				<input type="text" class="form-control" id="" placeholder="গ্রাহকের নাম"  required> -->
				<label for="validationCustom01">গ্রাহকের নাম</label>
				  <input type="text" class="form-control" name="data[Member][memberName]" id="validationCustom01" placeholder="গ্রাহকের নাম" value="" minlength="3"
					required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">পিতা/স্বামীর নাম</label>
				<input type="text" class="form-control" id="" placeholder="পিতা/স্বামীর নাম" name="data[Member][memberFatherName]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">মাতার নাম</label>
				<input type="text" class="form-control" id="" placeholder="মাতার নাম" name="data[Member][memberMotherName]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">জম্ম তারিখ</label>
				<input type="text" class="form-control" id="" placeholder="জম্ম তারিখ" name="data[Member][birthDate]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">বয়স</label>
				<input type="number" class="form-control" id="" placeholder="বয়স" name="data[Member][age]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">জাতীয় পরিচয় পত্র নং</label>
				<input type="text" class="form-control" id="" placeholder="জাতীয় পরিচয় পত্র নং"
					   name="data[Member][nationalIDNo]" minlength="17" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="occupation">পেশা</label>
				<select class="form-control " id="occupation" name="data[Member][occupation]" required>
					<option value="">পেশা</option>
					<option value="সরকারী চাকুরী">সরকারী চাকুরী</option>
					<option value="বেসরকারী চাকুরী">বেসরকারী চাকুরী</option>
					<option value="ব্যবসায়ী">ব্যবসায়ী</option>
					<option value="গৃহিনী">গৃহিনী</option>
					<option value="ছাত্র">ছাত্র</option>
					<option value="অনান্য">অনান্য</option>
				</select>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">মোবাইল নং</label>
				<input type="text" class="form-control" id="" placeholder="মোবাইল নং" name="data[Member][mobileNo]" minlength="11" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">ই-মেইল</label>
				<input type="email" class="form-control" id="" placeholder="ই-মেইল" name="data[Member][email]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
		</div>


	</div>
</div>
<!-- card1 end-->
<div class="card card-default"><!-- card2 -->
	<div class="card-header bg-primary">
		<h3 class="card-title">গ্রাহকের ঠিকানা</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row">
			<div class="form-group col-md-6">
				<label for="">বর্তমান ঠিকানা</label>
				<input type="text" class="form-control" id="" placeholder="বর্তমান ঠিকানা" name="data[Member][preAddress]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="">পোষ্ট অফিস</label>
				<input type="text" class="form-control" id="" placeholder="পোষ্ট অফিস" name="data[Member][prePost]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="">উপজেলা</label>
				<input type="text" class="form-control" id="" placeholder="উপজেলা" name="data[Member][preDistrict]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-6">
				<label for="">জেলা</label>
				<input type="text" class="form-control" id="" placeholder="জেলা" name="data[Member][preThana]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
		</div>
		<div class="card-body ">
			<input type="checkbox">বর্তমান ঠিকানা এবং স্থায়ী ঠিকানা একই
		</div>
		<div class="row">
			<div class="form-group col-md-6">
				<label for="">স্থায়ী ঠিকানা</label>
				<input type="text" class="form-control" id="" placeholder="স্থায়ী ঠিকানা" name="data[Member][perAddress]">
			</div>
			<div class="form-group col-md-6">
				<label for="">পোষ্ট অফিস</label>
				<input type="text" class="form-control" id="" placeholder="পোষ্ট অফিস" name="data[Member][perPost]">
			</div>
			<div class="form-group col-md-6">
				<label for="">উপজেলা</label>
				<input type="text" class="form-control" id="" placeholder="উপজেলা" name="data[Member][perDistrict]">
			</div>
			<div class="form-group col-md-6">
				<label for="">জেলা</label>
				<input type="text" class="form-control" id="" placeholder="জেলা" name="data[Member][perThana]">
			</div>
		</div>

	</div>
</div>
<!-- card2 end -->
<div class="card card-default"> <!-- card3 -->
	<div class="card-header bg-primary bg-primary">
		<h3 class="card-title">হিসাব খোলার তথ্য</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">হিসাবের ধরন</label>
				<select class="form-control" id="" name="data[Member][accounttype_id]" required>
					<option value="">হিসাবের ধরন</option>
					<?php
					#pr($accounttypes);die;
					foreach ($accounttypes as $key => $item) {
						?>
						<option value="<?= $key ?>"><?= $item ?></option>
					<?php
					}
					?>

				</select>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">হিসাব নং</label>
				<input type="text" class="form-control" id="" name="data[Account][accNo]" placeholder="হিসাব নং" minlength="4" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">খোলার তারিখ</label>
				<input type="date" class="form-control" id="" name="data[Account][accCreatedAt]" placeholder="খোলার তারিখ" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">মেয়াদ</label>
				<input type="text" class="form-control" id="" name="data[Account][period]" placeholder="মেয়াদ" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">সঞ্চয়ের পরিমান</label>
				<input type="number" step="0.01" class="form-control" id="" name="data[Account][storageAmmount]" placeholder="সঞ্চয়ের পরিমান" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">মোট জমা</label>
				<input type="number" step="0.01" class="form-control" id="" name="data[Account][totlaAmmount]" placeholder="মোট জমা">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">মুনাফা সহ সম্ভাব্য প্রাপ্য</label>
				<input type="number" step="0.01" class="form-control" id="" name="data[Account][rcvProfit]" placeholder="মুনাফা সহ সম্ভাব্য প্রাপ্য">
			</div>
			<div class="form-group col-md-4">
				<label for="">জমাকৃত টাকা</label>
				<input type="number" step="0.01" class="form-control" id="" name="data[Account][depositAmmount]" placeholder="জমাকৃত টাকা">
			</div>
			<div class="form-group col-md-4">
				<label for="">মেয়াদপূর্তির তারিখ</label>
				<input type="date" class="form-control" id="" name="data[Account][expiredDate]" placeholder="মেয়াদপূর্তির তারিখ">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">ভর্তি ফি</label>
				<input type="number" step="0.01" class="form-control" id="" name="data[Account][admissionFee]" placeholder="ভর্তি ফি">
			</div>
			<div class="form-group col-md-4">
				<label for="">উত্তোলন</label>
				<input type="number" step="0.01" class="form-control" id="" name="data[Account][withdraw]" placeholder="উত্তোলন">
			</div>
		</div>
	</div>
</div>
<!-- card3 end-->
<div class="card card-default"> <!-- card4 -->
	<div class="card-header bg-primary">
		<h3 class="card-title">পরিচয়দানকারীর তথ্য</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">এম ও কোড</label>
				<input class="mo" type="hidden" name="data[Staff][id][]">

				<input type="text" placeholder="এম ও কোড" class="form-control js-mo-code" list="mo_name" name="">
					<datalist  id="mo_name">

					<?php
					foreach ($staffs as $key => $stf) {
						//pr($stf);die;

						if(!empty($stf['Staff']['moCode'])) {
							?>
						<option
							value="<?= $stf['Staff']['name'] . '~' . $stf['Staff']['moCode'] . '~' . $stf['Staff']['phone'] ?>" data-id="<?php echo $stf['Staff']['id']; ?>"></option>
						<?php
						}
					}
					?>
					</datalist>
			</div>
			<div class="form-group col-md-4">
				<label for="">নাম</label>
				<input type="text" class="form-control name-js" placeholder="নাম">

			</div>
			<div class="form-group col-md-4">
				<label for="">মোবাইল নং</label>
				<input type="text" class="form-control mobile-js" placeholder="মোবাইল নং">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">পি ডি এম ই কোড</label>
				<input class="pdme" type="hidden" name="data[Staff][id][]">

				<input type="text" class="form-control js-pdme-code" id="" list="pdme_name" placeholder="পি ডি এম ই কোড">
					<datalist  id="pdme_name">

					<?php
					foreach ($staffs as $key => $stf) {
						//pr($stf);die;

						if(!empty($stf['Staff']['pdmeCode'])) {
							?>
						<option
							value="<?= $stf['Staff']['name'] . '~' . $stf['Staff']['pdmeCode'] . '~' . $stf['Staff']['phone'] ?>" data-id="<?php echo $stf['Staff']['id']; ?>"></option>
						<?php
						}
					}
					?>
					</datalist>
			</div>
			<div class="form-group col-md-4">
				<label for="">নাম</label>
				<input type="text" class="form-control aname-js" id="" placeholder="নাম">
			</div>
			<div class="form-group col-md-4">
				<label for="">মোবাইল নং</label>
				<input type="text" class="form-control amobile-js" id="" placeholder="মোবাইল নং">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">ডি এম ই কোড</label>
				<input class="dme" type="hidden" name="data[Staff][id][]">

				<input type="text" class="form-control js-dme-code" id="" list="dme_name" placeholder="ডি এম ই কোড">
					<datalist  id="dme_name">

					<?php
					foreach ($staffs as $key => $stf) {
						//pr($stf);die;

						if(!empty($stf['Staff']['dmeCode'])) {
							?>
						<option
							value="<?= $stf['Staff']['name'] . '~' . $stf['Staff']['dmeCode'] . '~' . $stf['Staff']['phone'] ?>" data-id="<?php echo $stf['Staff']['id']; ?>"></option>
						<?php
						}
					}
					?>
					</datalist>

			</div>
			<div class="form-group col-md-4">
				<label for="">নাম</label>
				<input type="text" class="form-control bname-js" id="" placeholder="নাম">
			</div>
			<div class="form-group col-md-4">
				<label for="">মোবাইল নং</label>
				<input type="text" class="form-control bmobile-js" id="" placeholder="মোবাইল নং">
			</div>
		</div>


	</div>
</div>
<!-- card4 end-->
<div class="card card-default"> <!-- card5 -->
	<div class="card-header bg-primary">
		<h3 class="card-title">নমিনির তথ্য</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">নমিনির নাম</label>
				<input type="text" class="form-control" id="" placeholder="নমিনির নাম" name="data[Member][nomName]" minlength="3" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">পিতার নাম</label>
				<input type="text" class="form-control" id="" placeholder="পিতার নাম" name="data[Member][nomFather]" minlength="3" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
			<div class="form-group col-md-4">
				<label for="">মাতার নাম</label>
				<input type="text" class="form-control" id="" placeholder="মাতার নাম" name="data[Member][nomMother]" required>
				<div class="valid-feedback feedback-icon">
					<i class="fa fa-check"></i>
				</div>
				<div class="invalid-feedback feedback-icon">
					<i class="fa fa-times"></i>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">বর্তমান ঠিকানা</label>
				<input type="text" class="form-control" id="" placeholder="জম্ম তারিখ"
					   name="data[Member][nomPreAddress]">
			</div>
			<div class="form-group col-md-4">
				<label for="">পোষ্ট অফিস</label>
				<input type="text" class="form-control" id="" placeholder="বয়স" name="data[Member][nomPost]">
			</div>
			<div class="form-group col-md-4">
				<label for="">থানা</label>
				<input type="text" class="form-control" id="" placeholder="থানা" name="data[Member][nomThana]">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">জেলা</label>
				<input type="text" class="form-control" id="" placeholder="জেলা" name="data[Member][nomDistrict]">
			</div>
			<div class="form-group col-md-4">
				<label for="">সম্পর্ক</label>
				<input type="text" class="form-control" id="" placeholder="সম্পর্ক" name="data[Member][relation]">
			</div>
			<div class="form-group col-md-4">
				<label for="">জম্ম তারিখ</label>
				<input type="text" class="form-control" id="" placeholder="জম্ম তারিখ"
					   name="data[Member][nomBirthDate]">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-4">
				<label for="">অংশীদার (%)</label>
				<input type="text" class="form-control" id="" placeholder="অংশীদার (%)"
					   name="data[Member][pertnerPercent]">
			</div>
			<div class="form-group col-md-4">
				<label for="">মোবাইল নং</label>
				<input type="text" class="form-control" id="" placeholder="মোবাইল নং" name="data[Member][nomMobile]">
			</div>
		</div>


	</div>
</div>
<!-- card5 end-->
<div class="card card-default"> <!-- card5 -->
	<div class="card-header bg-primary">
		<h3 class="card-title">তথ্য সংরক্ষন</h3>

		<div class="card-tools">
			<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
			<button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
		</div>
	</div>
	<!-- /.card-header -->
	<div class="card-body">
		<div class="row pt-4">
			<div class="form-group col-md-3">
				<label for="">গ্রাহকের ছবি</label>
				<input type="file" name="data[Member][memberPhoto]">
			</div>
			<div class="form-group col-md-3">
				<label for="">কার্ডের ছবি (jpg-৪০০*৬০০)</label>
				<input type="file" name="data[Member][cardPhoto]">
			</div>
			<div class="form-group col-md-3">
				<label for="">নমিনির ছবি</label>
				<input type="file" name="data[Member][nomPhoto]">
			</div>
			<div class="form-group col-md-3">
				<label for="">নমিনির স্বাক্ষর</label>
				<input type="file" class="" name="data[Member][nomSign]">
			</div>
		</div>

	</div>
	<!-- card5 end-->
</div>
<!-- /.card-body -->
<div class="row pt-4">
	<div class="form-group col-md-3">

		<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> সংরক্ষন করুন</button>
	</div>
	<div class="form-group col-md-3">
		<button type="button" class="btn btn-block btn-outline-secondary"><i class="fa fa-pencil"></i> আপডেট</button>
	</div>
	<div class="form-group col-md-3">
		<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-print"></i> প্রিন্ট</button>
	</div>
	<div class="form-group col-md-3">
		<button type="button" class="btn btn-block btn-outline-danger"><i class="fa fa-trash"></i> বাতিল করুন</button>
	</div>
</div>
<button type="submit" class="mt-2 btn btn-block btn-outline-primary col-md-4 offset-4">সঞ্চয়ী হিসাব খুলুন</button>
</div>
</form>
</div>
</div>
<!-- /.card-header -->
</div>


<!-- <div class="members form">
<?php echo $this->Form->create('Member'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Member'); ?></legend>
	<?php
echo $this->Form->input('memberName');
echo $this->Form->input('memberFatherName');
echo $this->Form->input('memberMotherName');
echo $this->Form->input('birthDate');
echo $this->Form->input('age');
echo $this->Form->input('nationalIDNo');
echo $this->Form->input('occupation');
echo $this->Form->input('mobileNo');
echo $this->Form->input('email');
echo $this->Form->input('preAddress');
echo $this->Form->input('prePost');
echo $this->Form->input('preDistrict');
echo $this->Form->input('preThana');
echo $this->Form->input('perAddress');
echo $this->Form->input('perPost');
echo $this->Form->input('perDistrict');
echo $this->Form->input('perThana');
echo $this->Form->input('nomName');
echo $this->Form->input('nomFather');
echo $this->Form->input('nomMother');
echo $this->Form->input('nomPreAddress');
echo $this->Form->input('nomPost');
echo $this->Form->input('nomThana');
echo $this->Form->input('nomDistrict');
echo $this->Form->input('relation');
echo $this->Form->input('nomBirthDate');
echo $this->Form->input('pertnerPercent');
echo $this->Form->input('nomMobile');
echo $this->Form->input('memberPhoto');
echo $this->Form->input('cardPhoto');
echo $this->Form->input('nomPhoto');
echo $this->Form->input('nomSign');
echo $this->Form->input('staff_id');
echo $this->Form->input('account_id');
echo $this->Form->input('accounttype_id');
?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Members'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounts'), array('controller' => 'accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('controller' => 'accounttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('controller' => 'accounttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->

<script>
	// Example starter JavaScript for disabling form submissions if there are invalid fields
	(function() {
		'use strict';
		window.addEventListener('load', function() {
			// Fetch all the forms we want to apply custom Bootstrap validation styles to
			var forms = document.getElementsByClassName('needs-validation');
			// Loop over them and prevent submission
			var validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
			});
		}, false);
	})();
</script>



<?php
/*echo $this->Html->script([
	'jquery.min.js',
	'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js',
]);*/
?>