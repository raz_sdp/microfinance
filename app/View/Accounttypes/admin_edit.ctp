<div class="accounttypes form">
<?php echo $this->Form->create('Accounttype'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Accounttype'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('accountType');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Accounttype.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Accounttype.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Accounts'), array('controller' => 'accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>
