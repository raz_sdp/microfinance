<div class="accounttypes view">
<h2><?php echo __('Accounttype'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($accounttype['Accounttype']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AccountType'); ?></dt>
		<dd>
			<?php echo h($accounttype['Accounttype']['accountType']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($accounttype['Accounttype']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($accounttype['Accounttype']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Accounttype'), array('action' => 'edit', $accounttype['Accounttype']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Accounttype'), array('action' => 'delete', $accounttype['Accounttype']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $accounttype['Accounttype']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounts'), array('controller' => 'accounts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Accounts'); ?></h3>
	<?php if (!empty($accounttype['Account'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Member Id'); ?></th>
		<th><?php echo __('Accounttype Id'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('AccNo'); ?></th>
		<th><?php echo __('AccCreatedAt'); ?></th>
		<th><?php echo __('Period'); ?></th>
		<th><?php echo __('StorageAmmount'); ?></th>
		<th><?php echo __('TotlaAmmount'); ?></th>
		<th><?php echo __('RcvProfit'); ?></th>
		<th><?php echo __('DepositAmmount'); ?></th>
		<th><?php echo __('ExpiredDate'); ?></th>
		<th><?php echo __('AdmissionFee'); ?></th>
		<th><?php echo __('Withdraw'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($accounttype['Account'] as $account): ?>
		<tr>
			<td><?php echo $account['id']; ?></td>
			<td><?php echo $account['member_id']; ?></td>
			<td><?php echo $account['accounttype_id']; ?></td>
			<td><?php echo $account['modified']; ?></td>
			<td><?php echo $account['accNo']; ?></td>
			<td><?php echo $account['accCreatedAt']; ?></td>
			<td><?php echo $account['period']; ?></td>
			<td><?php echo $account['storageAmmount']; ?></td>
			<td><?php echo $account['totlaAmmount']; ?></td>
			<td><?php echo $account['rcvProfit']; ?></td>
			<td><?php echo $account['depositAmmount']; ?></td>
			<td><?php echo $account['expiredDate']; ?></td>
			<td><?php echo $account['admissionFee']; ?></td>
			<td><?php echo $account['withdraw']; ?></td>
			<td><?php echo $account['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'accounts', 'action' => 'view', $account['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'accounts', 'action' => 'edit', $account['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'accounts', 'action' => 'delete', $account['id']), array('confirm' => __('Are you sure you want to delete # %s?', $account['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Account'), array('controller' => 'accounts', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
