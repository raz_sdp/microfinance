<div class="designations view">
<h2><?php echo __('Designation'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($designation['Designation']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Designation'); ?></dt>
		<dd>
			<?php echo h($designation['Designation']['designation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($designation['Designation']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($designation['Designation']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Designation'), array('action' => 'edit', $designation['Designation']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Designation'), array('action' => 'delete', $designation['Designation']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $designation['Designation']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Designations'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Designation'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Staffs'); ?></h3>
	<?php if (!empty($designation['Staff'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('NationalIDNo'); ?></th>
		<th><?php echo __('BloodGrp'); ?></th>
		<th><?php echo __('Father'); ?></th>
		<th><?php echo __('Mother'); ?></th>
		<th><?php echo __('Salary'); ?></th>
		<th><?php echo __('JoiningDate'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('Designation Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($designation['Staff'] as $staff): ?>
		<tr>
			<td><?php echo $staff['id']; ?></td>
			<td><?php echo $staff['code']; ?></td>
			<td><?php echo $staff['name']; ?></td>
			<td><?php echo $staff['address']; ?></td>
			<td><?php echo $staff['phone']; ?></td>
			<td><?php echo $staff['email']; ?></td>
			<td><?php echo $staff['nationalIDNo']; ?></td>
			<td><?php echo $staff['bloodGrp']; ?></td>
			<td><?php echo $staff['father']; ?></td>
			<td><?php echo $staff['mother']; ?></td>
			<td><?php echo $staff['salary']; ?></td>
			<td><?php echo $staff['joiningDate']; ?></td>
			<td><?php echo $staff['group_id']; ?></td>
			<td><?php echo $staff['designation_id']; ?></td>
			<td><?php echo $staff['created']; ?></td>
			<td><?php echo $staff['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'staffs', 'action' => 'view', $staff['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'staffs', 'action' => 'edit', $staff['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'staffs', 'action' => 'delete', $staff['id']), array('confirm' => __('Are you sure you want to delete # %s?', $staff['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
