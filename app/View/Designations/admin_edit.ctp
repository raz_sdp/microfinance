<div class="designations form">
<?php echo $this->Form->create('Designation'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Designation'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('designation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Designation.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Designation.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Designations'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Staffs'), array('controller' => 'staffs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Staff'), array('controller' => 'staffs', 'action' => 'add')); ?> </li>
	</ul>
</div>
