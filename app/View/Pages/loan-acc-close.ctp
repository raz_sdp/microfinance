
	<div class="card">
		<div class="card-body">
			<div class="card card-default bg-secondary">
				<div class="card-header mx-auto">
					<h3 class="card-title">লোন হিসাব বন্ধ করুন</h3>
				</div>
			</div>
<!-- /.card-header -->
<!-- form start -->
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					  <div class="card-header bg-primary">
						<h3 class="card-title">লোন হিসাব বন্ধ করুন</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					  </div>
					  <!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">এম আর নং</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">তারিখ</label>
								<input type="date" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">হিসাব নং</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">হিসাবের ধরন</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">গ্রাহকের নাম</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">পরিশোধের ধরন</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">বিবরন</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">কিস্তি</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">বিয়োগ</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">ব্যালান্স</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">ক্লোজিং ব্যালান্স</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">মোট টাকার পরিমান</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
						</div>
					<div class="row">
						<div class="form-group col-md-2 offset-1">
							<button type="button" class="btn btn-block btn-outline-danger"> <i class="fa fa-close"></i> বন্ধ করুন</button>
							
						</div>
						<div class="form-group col-md-2">
							<button type="button" class="btn btn-block btn-outline-success"> <i class="fa fa-edit"></i> আপডেট</button>
							
						</div>
						<div class="form-group col-md-2">
							<button type="button" class="btn btn-block btn-outline-danger"> <i class="fa fa-trash"></i> ডিলিট</button>
							
						</div>
						
						<div class="form-group col-md-2">
							<button type="button" class="btn btn-block btn-outline-primary"> <i class="fa fa-print"></i> প্রিন্ট</button>
						</div>
						<div class="form-group col-md-2">
							<button type="button" class="btn btn-block btn-outline-danger"> <i class="fa fa-undo"></i> বের হোন</button>
						</div>
					</div>

					
					</div>
				</div><!-- card1 end-->
				
			</div><!-- /.card-body -->
		</form>
	</div>
</div>
