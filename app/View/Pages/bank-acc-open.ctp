<div class="card">
	<div class="card-body">
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">ব্যাংক অ্যাকাউন্ট তৈরি করুন</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="bankName">ব্যাংকের নাম</label>
								<input type="text" class="form-control" id="bankName" placeholder="ব্যাংকের নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="branch" >শাখা</label>
								<input type="text" class="form-control" id="branch" placeholder="শাখা">
							</div>
							<div class="form-group col-md-4">
								<label for="accNo" >হিসাব নং</label>
								<input type="text" class="form-control" id="accNo" placeholder="হিসাব নং">
							</div>
						</div> 
						<div class="row">
							<div class="form-group col-md-6">
								<label for="amount">প্রধান ব্যালেন্স</label>
								<input type="text" class="form-control" id="amount" placeholder="প্রধান ব্যালেন্স">
							</div>
							<div class="form-group col-md-6">
								<label for="" >ধরন</label>
								<select class="form-control">
									<option>নির্বাচন করুন</option>
									<option>বর্তমান অ্যাকাউন্ট</option>
									<option>লোন অ্যাকাউন্ট</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body pt-0">
					<div class="row">
						<div class="col-md-4"></div>
						<div class="form-group col-md-4">
							<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-plus"></i> ব্যাংক অ্যাকাউন্ট যোগ করুন</button>
						</div>
					</div>
				</div><!-- card5 end-->	
			</div>
		</form>
		<hr>
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">ব্যাংকের তালিকা</h3>
							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 150px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">
									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small text-center">
								<tr>
									<th>ক্রঃনং</th>
									<th>ধরন</th>
									<th>ব্যাংক</th>
									<th>শাখা</th>
									<th>অ্যাকাউন্ট</th>
									<th>শুরু</th>
									<th>বর্তমান</th>
									<th>অবস্থা</th>
									<th>এডিট</th>
									<th>পেমেন্ট</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>Nayeem</td>
									<td>454</td>
									<td>454</td>
									<td>active</td>
									<td>active</td>
									<td>active</td>
									<td><button class="btn-outline-primary"><i class="fa fa-edit"></i></button></td>
									<td><button class="btn-outline-primary"><i class="fa fa-money"></i></button></td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</div>
</div>