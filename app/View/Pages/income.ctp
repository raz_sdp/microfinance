<div class="card-body">
		<div class="card card-default">
			<div class="card-header bg-secondary ">
				<h3 class="card-title text-center">গ্রহণ / আদায় / আয়</h3>
			</div>
		<div class="row">
			<div class="col-md-8">
				<form role="form">
					<div class="card-body">
						<div class="card card-default"> <!-- card1 -->
							  <div class="card-header bg-primary">
								<h3 class="card-title">গ্রাহকের তথ্য:</h3>
							  </div>
							  <!-- /.card-header -->

							<div class="card-body">
								<div class="row">
									<div class="form-group col-md-4">
										<label for="">তারিখ</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-4">
										<label for="">ক্যাশ / ব্যাংক:</label>
										<select class="form-control">
											<option>নির্বাচন করুন</option>
											<option value="ক্যাশ">ক্যাশ</option>
											<option value="ব্যাংক">ব্যাংক</option>
										</select>
									</div>
									<div class="form-group col-md-4">
										<label for="">আয়</label>
										<select class="form-control">
											<option value="0">নির্বাচন করুন</option>
											<option value="3">Building</option>
											<option value="4">My Capital</option>
											<option value="5">Equipment</option>
											<option value="10">Sales Income</option>
											<option value="12">Accounts Receivable</option>
											<option value="13">Accounts Payable</option>
											<option value="14">Mobile Bill</option>
											<option value="15">Prepaid Expense</option>
											<option value="16">Sales Returns and Allowances</option>
											<option value="17">Sales Discounts</option>
											<option value="18">Product Purchase</option>
											<option value="19">Purchase Return and Allowances</option>
											<option value="20">Purchase Discount</option>
											<option value="21">Drawings</option>
											<option value="22">Service Revenue</option>
											<option value="23">Salary Expense</option>
											<option value="40">MS</option>
											<option value="41">YS</option>
											<option value="42">HSS</option>
											<option value="43">FD</option>
											<option value="44">MP</option>
											<option value="45">GS</option>
											<option value="46">Accopen Fee</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label for="">টাকার পরিমান</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									<div class="form-group col-md-6">
										<input type="checkbox" name="">
										<label for="">চেক নম্বর</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
								</div>
								<div class="row">
									
									<div class="form-group col-md-12">
										<label for="">বিবরন</label>
										<textarea class="form-control">
											
										</textarea>
									</div>
								</div>
								<div class="form-group col-md-4 offset-4">
									<button type="button" class="btn btn-block btn-outline-primary"><i class="fa fa-money"></i> নগদ গ্রহণ করুন </button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-4">
				<form role="form">
					<div class="card-body">
						<div class="card card-default"> <!-- card1 -->
							  <!-- /.card-header -->

							<div class="card-body">
								<div class="row">
									<div class="form-group col-md-12">
										<label for="">বর্তমান ব্যাল্যান্স(Dr)</label>
										<input type="text" class="form-control" id=""disabled>
									</div>
									
								</div>
								<div class="row">
									<div class="form-group col-md-12">
										<label for="">গ্রহণ করা হয় (Cr)</label>
										<input type="text" class="form-control" id="" disabled>
									</div>
									
								</div>
								
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>