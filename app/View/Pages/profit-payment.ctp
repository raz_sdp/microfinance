<div class="card">
	<div class="card-body">
		<div class="card card-default bg-secondary">
			<div class="card-header mx-auto">
				<h3 class="card-title "> লভ্যাংশ ব্যবস্থাপনা</h3>
			</div>
		</div>

		<!-- /.card-header -->
		<!-- form start -->
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">লভ্যাংশ পরিশোধ</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="paymentDate">শুরুর তারিখ</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="date" class="form-control" id="paymentDate" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label for="paymentDate">শেষের তারিখ</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="date" class="form-control" id="paymentDate" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
								</div>
							</div>
							<div class="form-group col-md-4">
								<label for="accountNo">ব্রাঞ্চ</label>
								<select class="form-control">
									<option>নির্বাচন করুন</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="payment">অ্যাকাউন্টে ধরন</label>
								<select class="form-control" >
									<option value="0">নির্বাচন করুন</option>
									<option value="GS">GS</option>
									<option value="DS">DS</option>
									<option value="MS">MS</option>
									<option value="PS">PS</option>
									<option value="PSP">PSP</option>
									<option value="GS">GS</option>
									<option value="CP">CP</option>
									<option value="WS">WS</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="">মেয়াদকাল</label>
								<select class="form-control" id="">
									<option value="">পেমেন্টের মেয়াদ</option>
										<option value=".25">3 Months</option>
										<option value=".5">6 Months</option>
										<option value=".75">9 Months</option>
										<option value="1">1 Year</option>
										<option value="2">2 Years</option>
										<option value="6">6 Years</option>
										<option value="7">7 Years</option>
									</select>
							</div>
							<div class="form-group col-md-4">
								<label for="payment">শতাংশ (%)</label>
								<input type="text" class="form-control" id="payment" placeholder="">
							</div>
							
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<button type="button" class="btn btn-block btn-outline-primary"><i class="fa fa-save"> </i> সংরক্ষন করুন</button>
							</div>

							<div class="form-group col-md-4">
								<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-refresh"> </i> রিফ্রেশ</button>
							</div>
							
							<div class="form-group col-md-4">
								<button type="button" class="btn btn-block btn-outline-danger"><i class="fa fa-close"> </i> বের হয়ে যান</button>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</form>
	</div>
</div>