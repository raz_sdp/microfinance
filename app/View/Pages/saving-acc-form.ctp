<div class="card-body">
	<div class="card card-default">
		<div class="card-header bg-secondary">
			<h3 class="card-title text-center">সঞ্চয়ী হিসাব খুলুন</h3>
		</div>
		<div class="card-body">
			<!-- form start -->
			<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					  <div class="card-header bg-primary">
						<h3 class="card-title">গ্রাহকের তথ্য</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					  </div>
					  <!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">গ্রাহকের নাম</label>
								<input type="text" class="form-control" id="" placeholder="গ্রাহকের নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">পিতা/স্বামীর নাম</label>
								<input type="text" class="form-control" id="" placeholder="পিতা/স্বামীর নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">মাতার নাম</label>
								<input type="text" class="form-control" id="" placeholder="মাতার নাম">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">জম্ম তারিখ</label>
								<input type="text" class="form-control" id="" placeholder="জম্ম তারিখ">
							</div>
							<div class="form-group col-md-4">
								<label for="">বয়স</label>
								<input type="text" class="form-control" id="" placeholder="বয়স">
							</div>
							<div class="form-group col-md-4">
								<label for="">জাতীয় পরিচয় পত্র নং</label>
								<input type="text" class="form-control" id="" placeholder="জাতীয় পরিচয় পত্র নং">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="occupation">পেশা</label>
								<select class="form-control" id="occupation" name="occupation">
								    <option value="0">পেশা</option>							
									<option value="Gov Service">সরকারী চাকুরী</option>
									<option value="Private Service">বেসরকারী চাকুরী</option>
									<option value="Business">ব্যবসায়ী</option>
									<option value="House Wife">গৃহিনী</option>
									<option value="Student">ছাত্র</option>
									<option value="Others">অনান্য</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
							<div class="form-group col-md-4">
								<label for="">ই-মেইল</label>
								<input type="text" class="form-control" id="" placeholder="ই-মেইল">
							</div>
						</div>
							
					
					</div>
				</div><!-- card1 end-->
				<div class="card card-default"><!-- card2 -->
					  <div class="card-header bg-primary">
						<h3 class="card-title">গ্রাহকের ঠিকানা</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					  </div>
					  <!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="">বর্তমান ঠিকানা</label>
								<input type="text" class="form-control" id="" placeholder="বর্তমান ঠিকানা">
							</div>
							<div class="form-group col-md-6">
								<label for="">পোষ্ট অফিস</label>
								<input type="text" class="form-control" id="" placeholder="পোষ্ট অফিস">
							</div>
							<div class="form-group col-md-6">
								<label for="">উপজেলা</label>
								<input type="text" class="form-control" id="" placeholder="উপজেলা">
							</div>
							<div class="form-group col-md-6">
								<label for="">জেলা</label>
								<input type="text" class="form-control" id="" placeholder="জেলা">
							</div>
						</div>
						<div class="card-body">
							<input type="checkbox">বর্তমান ঠিকানা এবং স্থায়ী ঠিকানা একই
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<label for="">স্থায়ী ঠিকানা</label>
								<input type="text" class="form-control" id="" placeholder="স্থায়ী ঠিকানা">
							</div>
							<div class="form-group col-md-6">
								<label for="">পোষ্ট অফিস</label>
								<input type="text" class="form-control" id="" placeholder="পোষ্ট অফিস">
							</div>
							<div class="form-group col-md-6">
								<label for="">উপজেলা</label>
								<input type="text" class="form-control" id="" placeholder="উপজেলা">
							</div>
							<div class="form-group col-md-6">
								<label for="">জেলা</label>
								<input type="text" class="form-control" id="" placeholder="জেলা">
							</div>
						</div>
									
					</div>
				</div><!-- card2 end -->
				<div class="card card-default"> <!-- card3 -->
					  <div class="card-header bg-primary bg-primary">
						<h3 class="card-title">হিসাব খোলার তথ্য</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					  </div>
					  <!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="accType">হিসাবের ধরন</label>
								<select class="form-control" id="accType" name="accType">
								    <option value="0">হিসাবের ধরন</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="">হিসাব নং</label>
								<input type="text" class="form-control" id="" placeholder="হিসাব নং">
							</div>
							<div class="form-group col-md-4">
								<label for="">খোলার তারিখ</label>
								<input type="date" class="form-control" id="" placeholder="খোলার তারিখ">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">মেয়াদ</label>
								<input type="text" class="form-control" id="" placeholder="মেয়াদ">
							</div>
							<div class="form-group col-md-4">
								<label for="">সঞ্চয়ের পরিমান</label>
								<input type="text" class="form-control" id="" placeholder="সঞ্চয়ের পরিমান">
							</div>
							<div class="form-group col-md-4">
								<label for="">মোট জমা</label>
								<input type="text" class="form-control" id="" placeholder="মোট জমা">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">মুনাফা সহ সম্ভাব্য প্রাপ্য</label>
								<input type="text" class="form-control" id="" placeholder="মুনাফা সহ সম্ভাব্য প্রাপ্য">
							</div>
							<div class="form-group col-md-4">
								<label for="">জমাকৃত টাকা</label>
								<input type="text" class="form-control" id="" placeholder="জমাকৃত টাকা">
							</div>
							<div class="form-group col-md-4">
								<label for="">মেয়াদপূর্তির তারিখ</label>
								<input type="date" class="form-control" id="" placeholder="মেয়াদপূর্তির তারিখ">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">ভর্তি ফি</label>
								<input type="text" class="form-control" id="" placeholder="ভর্তি ফি">
							</div>
							<div class="form-group col-md-4">
								<label for="">উত্তোলন</label>
								<input type="number" class="form-control" id="" placeholder="উত্তোলন">
							</div>

						</div>
							
					
					</div>
				</div><!-- card3 end-->
				<div class="card card-default"> <!-- card4 -->
					  <div class="card-header bg-primary">
						<h3 class="card-title">পরিচয়দানকারীর তথ্য</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					  </div>
					  <!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">এম ও কোড</label>
								<input type="text" class="form-control" id="" placeholder="এম ও কোড">
							</div>
							<div class="form-group col-md-4">
								<label for="">নাম</label>
								<input type="text" class="form-control" id="" placeholder="নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">পি ডি এম ই কোড</label>
								<input type="text" class="form-control" id="" placeholder="পি ডি এম ই কোড">
							</div>
							<div class="form-group col-md-4">
								<label for="">নাম</label>
								<input type="text" class="form-control" id="" placeholder="নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">ডি এম ই কোড</label>
								<input type="text" class="form-control" id="" placeholder="ডি এম ই কোড">
							</div>
							<div class="form-group col-md-4">
								<label for="">নাম</label>
								<input type="text" class="form-control" id="" placeholder="নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
						</div>
							
					
					</div>
				</div><!-- card4 end-->
				<div class="card card-default"> <!-- card5 -->
					  <div class="card-header bg-primary">
						<h3 class="card-title">নমিনির তথ্য</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					  </div>
					  <!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">নমিনির নাম</label>
								<input type="text" class="form-control" id="" placeholder="নমিনির নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">পিতার নাম</label>
								<input type="text" class="form-control" id="" placeholder="পিতার নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">মাতার নাম</label>
								<input type="text" class="form-control" id="" placeholder="মাতার নাম">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">বর্তমান ঠিকানা</label>
								<input type="text" class="form-control" id="" placeholder="জম্ম তারিখ">
							</div>
							<div class="form-group col-md-4">
								<label for="">পোষ্ট অফিস</label>
								<input type="text" class="form-control" id="" placeholder="বয়স">
							</div>
							<div class="form-group col-md-4">
								<label for="">থানা</label>
								<input type="text" class="form-control" id="" placeholder="থানা">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">জেলা</label>
								<input type="text" class="form-control" id="" placeholder="জেলা">
							</div>
							<div class="form-group col-md-4">
								<label for="">সম্পর্ক</label>
								<input type="text" class="form-control" id="" placeholder="সম্পর্ক">
							</div>
							<div class="form-group col-md-4">
								<label for="">জম্ম তারিখ</label>
								<input type="text" class="form-control" id="" placeholder="জম্ম তারিখ">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">অংশীদার (%)</label>
								<input type="text" class="form-control" id="" placeholder="অংশীদার (%)">
							</div>
							<div class="form-group col-md-4">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
						</div>
							
					
					</div>
				</div><!-- card5 end-->
				<div class="card card-default"> <!-- card5 -->
					  <div class="card-header bg-primary">
						<h3 class="card-title">তথ্য সংরক্ষন</h3>

						<div class="card-tools">
						  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					  </div>
					  <!-- /.card-header -->
					<div class="card-body">
						<div class="row pt-4">
							<div class="form-group col-md-3">
								<label for="">গ্রাহকের ছবি</label>
								<input type="file">
							</div>
							<div class="form-group col-md-3">
								<label for="">কার্ডের ছবি (jpg-৪০০*৬০০)</label>
								<input type="file">
							</div>
							<div class="form-group col-md-3">
								<label for="">নমিনির ছবি</label>
								<input type="file">
							</div>
							<div class="form-group col-md-3">
								<label for="">নমিনির স্বাক্ষর</label>
								<input type="file">
							</div>
						</div>
						
				</div><!-- card5 end-->	
			</div><!-- /.card-body -->
			<div class="row pt-4">
				<div class="form-group col-md-3">
					
					<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> সংরক্ষন করুন </button>
				</div>
				<div class="form-group col-md-3">
					<button type="button" class="btn btn-block btn-outline-secondary"><i class="fa fa-pencil"></i> আপডেট </button>
				</div>
				<div class="form-group col-md-3">
					<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-print"></i> প্রিন্ট </button>
				</div>
				<div class="form-group col-md-3">
					<button type="button" class="btn btn-block btn-outline-danger"><i class="fa fa-trash"></i> বাতিল করুন </button>
				</div>
			</div>
					<button type="submit" class="mt-2 btn btn-block btn-outline-primary col-md-4 offset-4">সঞ্চয়ী হিসাব খুলুন</button>							
			</div>
			</form>
		</div>
	</div>
</div>

