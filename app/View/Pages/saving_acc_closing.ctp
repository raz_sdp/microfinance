<div class="card-body">
	<div class="card card-default">
		<div class="card-header bg-secondary">
			<div class="row">
				<h3 class="col-md-7 card-title">সঞ্চয়ী হিসাব বন্ধ করুন</h3>
				<form class="col-md-5 form-inline">
					<div class="input-group input-group-sm">
						<input class="form-control form-control-navbar" type="search" placeholder="হিসাব বন্ধ করার জন্য খুজুন..." aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-navbar" type="submit">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
			<!-- /.card-header -->
			<!-- form start -->
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">সঞ্চয়ী হিসাব বন্ধ করুন</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">হিসাব নং</label>
								<input type="text" class="form-control" id="" placeholder="হিসাব নং">
							</div>
							<div class="form-group col-md-3">
								<label for="">হিসাবের ধরন</label>
								<input type="text" class="form-control" id="" placeholder="হিসাবের ধরন" disabled>
							</div>
							<div class="form-group col-md-3">
								<label for="">গ্রাহকের নাম</label>
								<input type="text" class="form-control" id="" placeholder="গ্রাহকের নাম" disabled>
							</div>
							<div class="form-group col-md-3">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং" disabled>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">পরিশোধের ধরন</label>
								<input type="text" class="form-control" id="" placeholder="পরিশোধের ধরন" disabled>
							</div>
							<div class="form-group col-md-3">
								<label for="">ব্যালান্স</label>
								<input type="text" class="form-control" id="" placeholder="ব্যালান্স" disabled>
							</div>
							<div class="form-group col-md-3">
								<label for="">ক্লোজিং ব্যালান্স</label>
								<input type="text" class="form-control" id="" placeholder="ক্লোজিং ব্যালান্স">
							</div>
							<div class="form-group col-md-3">
								<label for="">বিয়োগ</label>
								<input type="text" class="form-control" id="" placeholder="বিয়োগ">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">মোট পরিমান</label>
								<input type="text" class="form-control" id="" placeholder="মোট পরিমান" disabled>
							</div>
							<div class="form-group col-md-3">
								<label for="">তারিখ</label>
								<input type="text" class="form-control" id="" placeholder="তারিখ">
							</div>
							<div class="form-group col-md-6">
								<label for="">বিবরন</label>
								<input type="text" class="form-control" id="" placeholder="বিবরন">
							</div>
						</div>
					</div>
				</div><!-- card1 end-->
			</div><!-- /.card-body -->
			<div class="form-group col-md-4 offset-4">
				<button type="button" class="btn btn-block btn-outline-danger"><i class="fa fa-trash"></i> সঞ্চয়ী হিসাব বন্ধ করুন </button>
			</div>
		</form>
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">উত্তোলনকারী সদস্যদের তালিকা</h3>

							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 150px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">

									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small">
								<tr>
									<th>ক্রঃ নং</th>
									<th>এম আর নং</th>
									<th>হিসাব নং</th>
									<th>হিসাবের ধরন</th>
									<th>গ্রাহকের নাম</th>
									<th>মোবাইল নং</th>
									<th>ক্লোজিং ব্যালান্স</th>
									<th>বিয়োগ</th>
									<th>মোট</th>
									<th>বিবরন</th>
									<th>এডিট </th>
									<th>প্রিন্ট</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>Khulna</td>
									<td>1234567</td>
									<td>11-7-2014</td>
									<td>1357</td>
									<td>100000</td>
									<td>asdf</td>
									<td></td>
									<td></td>
									<td><i class="fa fa-pencil"></i> Edit</td>
									<td><i class="fa fa-print"></i> Print</td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</div>
</div>