<div class="card">
	<div class="card-body">
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">হিসাবরক্ষন যোগ করুন</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="accountHead">প্রধানের নাম নির্বাচন করুন</label>
								<select class="form-control" id="acc_type" name="acc_type">
									<option>নির্বাচন করুন</option>
									<option value="1">Current Assets</option>
									<option value="2">Fixed Asset</option>
									<option value="3">Capital Accounts</option>
									<option value="4">Loan</option>
									<option value="5">Sales</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="account">খতিয়ানের নাম</label>
								<input type="text" class="form-control" id="account" placeholder="খতিয়ানের নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="accType" >প্রারম্ভিক ব্যালেন্স</label>
								<input type="number" class="form-control" id="acctType" placeholder="প্রারম্ভিক ব্যালেন্স">
							</div>
							<div class="form-group col-md-12">
								<label class="control-label" for=""><input name="bank_cash" type="checkbox" value=""> Bank Or Cash Account</label>
							</div>
						</div> 
					</div>
				</div>
				<div class="card-body pt-0">
					<div class="row">
						<div class="col-md-4"></div>
						<div class="form-group col-md-4">
							<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-plus"></i> যোগ করুন</button>
						</div>
					</div>
				</div><!-- card5 end-->	
			</div>
		</form>
		<hr>
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">হিসাবরক্ষনের তালিকা</h3>
							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 150px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">
									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small text-center">
								<tr>
									<th>ক্রঃনং</th>
									<th>কোড</th>
									<th>খতিয়ানের নাম</th>
									<th>প্রারম্ভিক ব্যালেন্স</th>
									<th>বর্তমান ব্যালেন্স</th>
									<th>অবস্থা</th>
									<th>এডিট</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>Nayeem</td>
									<td>454</td>
									<td>454</td>
									<td>active</td>
									<td><button class="btn-outline-primary"><i class="fa fa-edit"></i></button></td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</div>
</div>