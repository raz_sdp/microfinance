<div class="container">
	<nav class="navbar navbar-expand-lg navbar-light bg-primary">
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
			  <li class="nav-item active">
				<a class="nav-link" href="gs-transaction">সঞ্চয়/কিস্তি আদায়<span class="sr-only">(current)</span></a>
			  </li>
			  <li class="nav-item ">
				<a class="nav-link" href="ds-transaction"> দৈনিক সঞ্চয় আদায়</a>
			  </li>
			  <li class="nav-item ">
				<a class="nav-link" href="saving-transaction"> মাসিক সঞ্চয় আদায়</a>
			  </li>
			  <li class="nav-item ">
				<a class="nav-link" href="ys-transaction"> বাৎসরিক সঞ্চয় আদায়</a>
			  </li>
			</ul>
		</div>
		<form class="form-inline ml-3">
		  <div class="input-group input-group-sm">
			<input class="form-control form-control-navbar" type="search" placeholder="গ্রাহক খুজুন" aria-label="Search">
			<div class="input-group-append">
			  <button class="btn btn-navbar" type="submit">
				<i class="fa fa-search"></i>
			  </button>
			</div>
		  </div>
		</form>
	</nav>
</div>