<div class="card">
	<div class="card-body">
		<!-- /.card-header -->
		<!-- form start -->
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">অ্যাকাউন্টের প্রধান যোগ করুন</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="account">অ্যাকাউন্টের নাম</label>
								<input type="text" class="form-control" id="account" placeholder="অ্যাকাউন্টের নাম">
							</div>
							<div class="form-group col-md-6">
								<label for="accType" >অ্যাকাউন্টের ধরন </label>
								<select class="form-control" id="acc_type" name="acc_type">
									<option>নির্বাচন করুন</option>
									<option value="1">Assets</option>
									<option value="2">Expenses</option>
									<option value="3">Incomes or Revenues</option>
									<option value="4">Liabilities and Owners Equity</option>
								</select>
							</div>
						</div> 
					</div>
				</div>
				<div class="card-body pt-0">
					<div class="row">
						<div class="col-md-4"></div>
						<div class="form-group col-md-4">
							<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-plus"></i> যোগ করুন</button>
						</div>
					</div>
				</div><!-- card5 end-->	
			</div>
		</form>
		<hr>
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">অ্যাকাউন্ট প্রধানের তালিকা</h3>
							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 200px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">
									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small text-center">
								<tr>
									<th>ক্রঃনং</th>
									<th>কোড</th>
									<th>অ্যাকাউন্টের নাম</th>
									<th>অ্যাকাউন্টের ধরন</th>
									<th>এডিট</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>3485930</td>
									<td>Khulna</td>
									<td><button class="btn-outline-primary"><i class="fa fa-edit"></i></button></td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</div>
</div>