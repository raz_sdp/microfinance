<div class="card-body">
	<div class="card card-default">
		<div class="card-header bg-secondary">
			<h3 class="card-title text-center">লোন হিসাব খুলুন</h3>
		</div>
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">গ্রাহকের তথ্য</h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<!-- <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">গ্রাহকের নাম</label>
								<input type="text" class="form-control" id="" placeholder="গ্রাহকের নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">পিতার নাম</label>
								<input type="text" class="form-control" id="" placeholder="পিতা/স্বামীর নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="">মাতার নাম</label>
								<input type="text" class="form-control" id="" placeholder="মাতার নাম">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">জম্ম তারিখ</label>
								<input type="date" class="form-control" id="" placeholder="জম্ম তারিখ">
							</div>
							<div class="form-group col-md-4">
								<label for="">বয়স</label>
								<input type="text" class="form-control" id="" placeholder="বয়স">
							</div>
							<div class="form-group col-md-4">
								<label for="">জাতীয় পরিচয় পত্র নং</label>
								<input type="text" class="form-control" id="" placeholder="জাতীয় পরিচয় পত্র নং">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="">পেশা</label>
								<input type="text" class="form-control" id="" placeholder="পেশা">
							</div>
							<div class="form-group col-md-4">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
							<div class="form-group col-md-4">
								<label for="">ই-মেইল</label>
								<input type="text" class="form-control" id="" placeholder="ই-মেইল">
							</div>
						</div>
					</div>
				</div><!-- card1 end-->
				<div class="card card-default"><!-- card2 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">গ্রাহকের ঠিকানা</h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<!-- <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="">বর্তমান ঠিকানা</label>
								<input type="text" class="form-control" id="" placeholder="বর্তমান ঠিকানা">
							</div>
							<div class="form-group col-md-6">
								<label for="">পোষ্ট অফিস</label>
								<input type="text" class="form-control" id="" placeholder="পোষ্ট অফিস">
							</div>
							<div class="form-group col-md-6">
								<label for="">উপজেলা</label>
								<input type="text" class="form-control" id="" placeholder="উপজেলা">
							</div>
							<div class="form-group col-md-6">
								<label for="">জেলা</label>
								<input type="text" class="form-control" id="" placeholder="জেলা">
							</div>
						</div>
						<div class="card-body">
							<input type="checkbox">বর্তমান ঠিকানা এবং স্থায়ী ঠিকানা একই
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<label for="">স্থায়ী ঠিকানা</label>
								<input type="text" class="form-control" id="" placeholder="স্থায়ী ঠিকানা">
							</div>
							<div class="form-group col-md-6">
								<label for="">পোষ্ট অফিস</label>
								<input type="text" class="form-control" id="" placeholder="পোষ্ট অফিস">
							</div>
							<div class="form-group col-md-6">
								<label for="">উপজেলা</label>
								<input type="text" class="form-control" id="" placeholder="উপজেলা">
							</div>
							<div class="form-group col-md-6">
								<label for="">জেলা</label>
								<input type="text" class="form-control" id="" placeholder="জেলা">
							</div>
						</div>
					</div>
				</div><!-- card2 end -->
				<div class="card card-default"> <!-- card3 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">লোন হিসাব খোলার তথ্য</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<!-- <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">লোন আই ডি নং</label>
								<input type="text" class="form-control" id="" placeholder="ex:012...">
							</div>
							<div class="form-group col-md-3">
								<label for="">সঞ্চয়(জমা)</label>
								<input type="text" class="form-control" id="" placeholder="সঞ্চয়(জমা)">
							</div>
							<div class="form-group col-md-3">
								<label for="">ঋণ সংখ্যা </label>
								<input type="text" class="form-control" id="" placeholder="ঋণ সংখ্যা">
							</div>
							<div class="form-group col-md-3">
								<label for="">লোনের পরিমান</label>
								<input type="text" class="form-control" id="" placeholder="লোনের পরিমান">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">10%(সঞ্চয়ী)</label>
								<input type="text" class="form-control" id="" placeholder="10%(সঞ্চয়ী)">
							</div>
							<div class="form-group col-md-3">
								<label for="">পরিশোধের ধরন</label>
								<input type="text" class="form-control" id="" placeholder="পরিশোধের ধরন">
							</div>
							<div class="form-group col-md-3">
								<label for="">সার্ভিস চার্জ(%)</label>
								<input type="text" class="form-control" id="" placeholder="মোট জমা">
							</div>
							<div class="form-group col-md-3">
								<label for="">মোট পরিমান</label>
								<input type="text" class="form-control" id="" placeholder="মোট পরিমান">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">কিস্তি সংখ্যা</label>
								<input type="text" class="form-control" id="" placeholder="কিস্তি সংখ্যা">
							</div>
							<div class="form-group col-md-3">
								<label for="">কিস্তি</label>
								<input type="text" class="form-control" id="" placeholder="কিস্তি">
							</div>
							<div class="form-group col-md-3">
								<label for="">সমিতি/গ্র“প:</label>
								<input type="text" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-3">
								<label for="">খোলার তারিখ</label>
								<input type="date" class="form-control" id="" placeholder="">
							</div>
						</div>
					</div>
				</div><!-- card3 end-->
				<div class="card card-default"> <!-- card4 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">জামিনদার</h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<!-- <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body pt-0">
						<center><h3 class="p-4 card-title">জামিনদার ১ এর তথ্য</h3></center>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">হিসাব নং</label>
								<input type="text" class="form-control" id="" placeholder="এম ও কোড">
							</div>
							<div class="form-group col-md-3">
								<label for="">জামিনদারের নাম</label>
								<input type="text" class="form-control" id="" placeholder="নাম">
							</div>
							<div class="form-group col-md-3">
								<label for="">বর্তমান ঠিকানা</label>
								<input type="text" class="form-control" id="" placeholder="পি ডি এম ই কোড">
							</div>
							<div class="form-group col-md-3">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
						</div>
						<center><h3 class="p-4 card-title">জামিনদার ২ এর তথ্য</h3></center>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="">হিসাব নং</label>
								<input type="text" class="form-control" id="" placeholder="এম ও কোড">
							</div>
							<div class="form-group col-md-3">
								<label for="">জামিনদারের নাম</label>
								<input type="text" class="form-control" id="" placeholder="নাম">
							</div>
							<div class="form-group col-md-3">
								<label for="">বর্তমান ঠিকানা</label>
								<input type="text" class="form-control" id="" placeholder="পি ডি এম ই কোড">
							</div>
							<div class="form-group col-md-3">
								<label for="">মোবাইল নং</label>
								<input type="text" class="form-control" id="" placeholder="মোবাইল নং">
							</div>
						</div>
					</div>
				</div><!-- card4 end-->
				<div class="card card-default"> <!-- card5 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">ছবি এবং স্বাক্ষর</h3>
						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<!-- <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-6">
								<label for="">গ্রাহকের ছবি</label>
								<input type="file" class="form-control" id="" placeholder="">
							</div>
							<div class="form-group col-md-6">
								<label for="">গ্রাহকের স্বাক্ষর</label>
								<input type="file" class="form-control" id="" placeholder="">
							</div>
						</div>
					</div>
				</div><!-- card5 end-->
				<div class="row">
					<div class="form-group col-md-4">
						<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> সংরক্ষন করুন</button>
					</div>
					<div class="form-group col-md-4">
						<button type="button" class="btn btn-block btn-outline-primary"><i class="fa fa-print"></i> প্রিন্ট</button>
					</div>
					<div class="form-group col-md-4">
						<button type="button" class="btn btn-block btn-outline-danger"><i class="fa fa-trash"></i> বাতিল করুন</button>
					</div>
				</div>
			</div><!-- card5 end-->	
		</form>
	</div>
</div>