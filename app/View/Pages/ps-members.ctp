<div class="card-body">
	<div class="card card-default">
		<div class="card-header bg-secondary p-0">
			<nav class="navbar navbar-expand-lg navbar-light">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="member-list"> সাধারণ সঞ্চয়ী হিসাব <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="ds-members"> দৈনিক সঞ্চয়ী হিসাব </a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="ms-members"> মাসিক সঞ্চয়ী হিসাব </a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="ys-members"> বাৎসরিক সঞ্চয়ী হিসাব </a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="psp-members"> পি এস পি </a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="ps-members"> পি এস </a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="loan-members"> লোন </a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">মাসিক মুনফাযুক্ত সঞ্চয়ী সদস্যের তালিকা</h3>

							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 150px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">

									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small">
								<tr>
									<th>ক্রঃনং</th>
									<th>হিসাব নং</th>
									<th>নাম</th>
									<th>ঠিকানা</th>
									<th>উপজেলা</th>
									<th>মোবাইল নং.</th>
									<th>খোলার তাং</th>
									<th>মেয়াদ(বছর)</th>
									<th>জমাকৃত</th>
									<th>মাসিক লভাংশ্য</th>
									<th>উত্তোলন</th>
									<th>এডিট</th>
									<th>সঞ্চয় আদায়</th>
									<th>প্রিন্ট</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>Nayeem</td>
									<td>Khulna</td>
									<td>Khulna</td>
									<td>01924161357</td>
									<td>11-7-2014</td>
									<td>5</td>
									<td>1000</td>
									<td>4353465</td>
									<td>50000</td>
									<td>Edit</td>
									<td>1200</td>
									<td>Print</td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</div>
</div>