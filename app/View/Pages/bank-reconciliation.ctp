<div class="card">
	<div class="card-body">
		<div class="card card-default bg-secondary">
			<div class="card-header mx-auto">
				<h3 class="card-title">ব্যাংক রিকনসুলেশন</h3>
			</div>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">মুলতুবি চেকের তালিকা</h3>
							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 200px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">
									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small text-center">
								<tr>
									<th>ক্রঃনং</th>
									<th>চেক নং</th>
									<th>টাকার পরিমান</th>
									<th>বর্ননা</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>3485930</td>
									<td>Khulna</td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">অনুমোদিত চেকের তালিকা</h3>
							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 200px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">
									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small text-center">
								<tr>
									<th>ক্রঃনং</th>
									<th>চেক নং</th>
									<th>টাকার পরিমান</th>
									<th>বর্ননা</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>3485930</td>
									<td>Khulna</td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">বাতিল চেকের তালিকা</h3>
							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 200px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">
									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
										<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small text-center">
								<tr>
									<th>ক্রঃনং</th>
									<th>চেক নং</th>
									<th>টাকার পরিমান</th>
									<th>বর্ননা</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>3485930</td>
									<td>Khulna</td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</div>
</div>