<div class="card-body">
  <div class="card card-default">
    <div class="card-header bg-secondary p-0">
      <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="member-list"> সাধারণ সঞ্চয়ী হিসাব <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="ds-members"> দৈনিক সঞ্চয়ী হিসাব </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="ms-members"> মাসিক সঞ্চয়ী হিসাব </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="ys-members"> বাৎসরিক সঞ্চয়ী হিসাব </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="psp-members"> পি এস পি </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="ps-members"> পি এস </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="loan-members"> লোন </a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
      <div class="card-body">
        Under Construction
      </div>
  </div>
</div>