<div class="card">
	<div class="card-body">
		<div class="card card-default">
			<div class="card-header bg-secondary ">
				<h3 class="card-title">Admin Panel Settings</h3>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-12">
						<!-- Custom Tabs -->
						<div class="card-body">
							<div class="card">
								<div class="card-header d-flex p-0">
									<ul class="nav nav-pills ml-auto p-2">
										<li class="nav-item"><a class="nav-link active show" href="#tab_1" data-toggle="tab">Company Setting</a></li>
										<li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Username/Password</a></li>
										<li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab">Access Control</a></li>
										<li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab">Theme Change</a></li>
										<li class="nav-item"><a class="nav-link" href="#tab_5" data-toggle="tab">Print Format</a></li>
										<li class="nav-item"><a class="nav-link" href="#tab_6" data-toggle="tab">Database Backup</a></li>                  
									</ul>
								</div><!-- /.card-header -->
								<div class="card-body">
									<div class="tab-content">
										<div class="tab-pane active show" id="tab_1">
											<div class="card card-default">
												<div class="card-header bg-primary ">
													<h3 class="card-title">Admin Panel Settings</h3>
												</div>
												<form role="form">
													<div class="card-body">
														<div class="row">
															<div class="form-group col-md-3">
																<label for="">Name</label>
																<input type="text" class="form-control" id="" >
															</div>
															<div class="form-group col-md-3">
																<label for="">Title</label>
																<input type="text" class="form-control" id="">
															</div>
															<div class="form-group col-md-3">
																<label for="">logo</label>
																<input type="file" class="form-control" id="" >
															</div>
															<div class="form-group col-md-3">
																<label for="">Address</label>
																<input type="text" class="form-control" id="" >
															</div>
														</div>
														<div class="row pt-4">
															<div class="form-group col-md-4 offset-4">
																<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> Update </button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
										
										<!-- /.tab-pane -->

										<div class="tab-pane" id="tab_2">
											<div class="card card-default">
												<div class="card-header bg-primary ">
													<h3 class="card-title">Admin Panel Settings</h3>
												</div>
												<form role="form">
													<div class="card-body">
														<div class="row">
															<div class="form-group col-md-3">
																<label for="">Select User</label>
																<select class="form-control">
																	<option>admin</option>
																</select>
															</div>
															<div class="form-group col-md-3">
																<label for="">New Username</label>
																<input type="text" class="form-control" id="">
															</div>
															<div class="form-group col-md-3">
																<label for="">New Password</label>
																<input type="password" class="form-control" id="" >
															</div>
															<div class="form-group col-md-3">
																<label for="">Confirm Password</label>
																<input type="password" class="form-control" id="" >
															</div>
														</div>
														<div class="row pt-4">
															<div class="form-group col-md-4 offset-4">
																<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> Create </button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="tab_3">
											<div class="card card-default">
												<div class="card-header bg-primary ">
													<h3 class="card-title">Access Panel Settings</h3>
												</div>
												<form role="form">
													<div class="form-group">
														<div class="col-sm-8 offset-2 ">
															<div class="panel panel-default">
																<!-- Default panel contents -->
																<div class="form-group col-md-6 offset-3 pt-2">
																	<label for="">Select User:</label>
																	<select class="form-control" id="" name="">
																		<option value="">Admin</option>
																	</select>
																</div>
																<!-- List group -->
																<ul class="list-group pt-2">
																			  
																	<li class="list-group-item">
																		Member Detail Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="dashboard" name="" type="checkbox" value="" checked="checked">
																			<label for="dashboard" class="label-success"></label>
																		</div>
																	</li>
																	<li class="list-group-item">
																		Loan Account Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="sales" name="someSwitchOption001" type="checkbox" checked="checked">
																			<label for="sales" class="label-success"></label>
																		</div>
																	</li>
																	<li class="list-group-item">
																		Saving Account Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="all_purchase" name="" type="checkbox" checked="checked">
																			<label for="all_purchase" class="label-success"></label>
																		</div>
																	</li>
																	<li class="list-group-item">
																		H/O Accounts Open Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="purchase_item" name="someSwitchOption001" type="checkbox" checked="checked">
																			<label for="purchase_item" class="label-success"></label>
																		</div>
																	</li>
																	 <li class="list-group-item">
																		Staff Management Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="staffs" name="someSwitchOption001" type="checkbox" checked="checked">
																			<label for="staffs" class="label-success"></label>
																		</div>
																	</li>
																	<li class="list-group-item">
																		Profit Management Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="suppliers" name="someSwitchOption001" type="checkbox" checked="checked">
																			<label for="suppliers" class="label-success"></label>
																		</div>
																	</li>
																	<li class="list-group-item">
																		Setting Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="customers" name="someSwitchOption001" type="checkbox" checked="checked">
																			<label for="customers" class="label-success"></label>
																		</div>
																	</li>
																	<li class="list-group-item">
																	   General Account Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="accounts" name="someSwitchOption001" type="checkbox" checked="checked">
																			<label for="accounts" class="label-success"></label>
																		</div>
																	</li>
																	<li class="list-group-item">
																		Reports Access
																		<div class="material-switch pull-right">
																			<input class="all_input" id="reports" name="someSwitchOption001" type="checkbox" checked="checked">
																			<label for="reports" class="label-success"></label>
																		</div>
																	</li>    
																</ul>
															</div>
														</div>
													</div>
													<div class="row pt-4">
														<div class="form-group col-md-4 offset-4">
															<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> Create </button>
														</div>
													</div>
												</form>
											</div>
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="tab_4">
											<p>Theme Under Construction!!Get Next Update V.2.0</p>
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="tab_5">
											<p>Print Under Construction!!Get Next Update V.2.0</p>
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="tab_6">
											<p>Database Under Construction!!Get Next Update V.2.0 </p>
										</div>
										<!-- /.tab-pane -->
									</div>
									<!-- /.tab-content -->
								</div><!-- /.card-body -->
							</div>
						</div>
						<!-- ./card -->
					</div>
					<!-- /.col -->
				</div>
			</div>
		</div>
	</div>
</div>