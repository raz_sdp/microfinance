<div class="card">
	<div class="card-body">
		<div class="card card-default">
      <div class=" card-header p-2 bg-secondary">
        <div class="row">
          <div class="col-md-6">
            <h3 class="card-title pt-2 pl-3">ষ্টাফদের তথ্য</h3>
          </div>
          <div class="col-md-6">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#desigmodal">
              পদবী যোগ করুন
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#grpModal">
              গ্রুপ যোগ করুন
            </button>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#stuffModal">
              ষ্টাফ যোগ করুন
            </button>
            <!-- Modal -->
            <div class="modal fade" id="desigmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLongTitle">পদবী যোগ করুন</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body text-dark">
                    <div class="text-success d-none text-center" id="success" >
                      <p>পদবী যোগ সম্পন্ন হয়েছে</p>
                    </div>
                    <p class="text-danger d-none text-center js-fail-msg">পদবী যোগ সম্পন্ন হয়নি আবার চেষ্টা করুন</p>
                    <h6>পদবীর তালিকা</h6>
                    <ul class="list-group js-desig-group">
                      <?php foreach ($designations as $key => $designation): ?>
                        <li class="list-group-item list-group-item-primary"><?=$designation['Designation']['designation']?></li>
                      <?php endforeach ?>
                    </ul>
                      <div class="form-group">
                        <hr>
                        <label for="desig" >পদবীর নাম :</label>
                        <input type="text" class="form-control js-designation-inp" id="desig" placeholder="পদবীর নাম">
                      </div>
                      <div class="form-group text-center">
                        <button type="button" class="btn btn-outline-info js-add-designation">যোগ করুন</button>
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">বন্ধ করুন</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal fade" id="grpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title text-dark" id="exampleModalLongTitle">গ্রুপ যোগ করুন</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="container pt-2">
                      <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-header bg-primary">
                              <h3 class="card-title">গ্রুপের তালিকা</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                              <table class="table table-hover table-dark small">
                                <tr>
                                  <th>গ্রুপ কোড</th>
                                  <th>গ্রুপ নাম</th>
                                  <th>সভানেত্রী</th>
                                  <th>গ্রাম</th>
                                </tr>
                                <tr>
                                  <td>1434</td>
                                  <td>shapla</td>
                                  <td>someone</td>
                                  <td>Batiaghata</td>
                                </tr>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                    <form role="form">
                      <div class="card-body text-dark">
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label for="code" >গ্রুপ কোড :</label>
                            <input type="text" class="form-control" id="code" placeholder="গ্রুপ কোড">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="name" >গ্রুপ নাম :</label>
                            <input type="text" class="form-control" id="name" placeholder="গ্রুপ নাম ">
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-6">
                            <label for="leader" >সভানেত্রী:</label>
                            <input type="text" class="form-control" id="leader" placeholder="সভানেত্রী">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="village" >গ্রাম :</label>
                            <input type="text" class="form-control" id="village" placeholder="গ্রাম">
                          </div>
                        </div>
                      </div>
                      <div class="form-group text-center">
                        <button type="submit" class="btn btn-outline-info">যোগ করুন</button>
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">বন্ধ করুন</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="modal fade bd-example-modal-lg" id="stuffModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-dark" id="myLargeModalLabel">নতুন ষ্টাফ যোগ করুন</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body ">
                  <form role="form">
                      <div class="card-body text-dark">
                        <div class="row">
                          <div class="form-group col-md-4">
                            <label for="code" >কোড:</label>
                            <input type="text" class="form-control" id="code" placeholder="কোড">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="name" >নাম:</label>
                            <input type="text" class="form-control" id="name" placeholder="নাম">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="address" >ঠিকানা:</label>
                            <input type="text" class="form-control" id="address" placeholder="ঠিকানা">
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-4">
                            <label for="phone" >ফোন :</label>
                            <input type="text" class="form-control" id="phone" placeholder="ফোন ">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="mail" >ই-মেইল:</label>
                            <input type="text" class="form-control" id="mail" placeholder="ই-মেইল">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="idno" >জাতীয় পরিচয় নং:</label>
                            <input type="text" class="form-control" id="idno" placeholder="জাতীয় পরিচয় নং">
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-4">
                            <label for="blood" >রক্তের গ্রুপ:</label>
                            <select class="form-control" id="blood">
                              <option>A+</option>
                              <option>A-</option>
                              <option>B+</option>
                              <option>B-</option>
                              <option>O+</option>
                              <option>O-</option>
                              <option>AB+</option>
                              <option>AB-</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="father" >পিতার নাম:</label>
                            <input type="text" class="form-control" id="father" placeholder="পিতার নাম">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="mother" >মাতার নাম:</label>
                            <input type="text" class="form-control" id="mother" placeholder="মাতার নাম">
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-4">
                            <label for="deignation" >পদবী নির্বাচন করুন:</label>
                            <select class="form-control" id="deignation">
                              <option>MO</option>
                              <option>PDME</option>
                              <option>DME</option>
                            </select>
                          </div>
                          <div class="form-group col-md-4">
                            <label for="sallary" >বেতন:</label>
                            <input type="text" class="form-control" id="sallary" placeholder="বেতন" value="0">
                          </div>
                          <div class="form-group col-md-4">
                            <label for="date" >যোগদানের তারিখ:</label>
                            <input type="date" class="form-control" id="date" placeholder="যোগদানের তারিখ">
                          </div>
                        </div>
                        <div class="row">
                          <div class="form-group col-md-12">
                            <label for="group" >গ্রুপ:</label>
                            <input type="text" class="form-control" id="group" placeholder="গ্রুপ">
                          </div>
                        </div>
                      </div>
                      <div class="form-group text-center">
                        <button type="submit" class="btn btn-outline-info">যোগ করুন</button>
                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">বন্ধ করুন</button>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      <div class="container pt-2">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header bg-primary">
                <h3 class="card-title">ষ্টাফদের তালিকা</h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover small">
                  <tr>
                    <th>ক্রঃনং</th>
                    <th>আই ডি</th>
                    <th>গ্রুপ আই ডি</th>
                    <th>নাম</th>
                    <th>পদবী</th>
                    <th>বেতন</th>
                    <th>যোগদানের তারিখ</th>
                    <th>অবস্থা</th>
                    <th>ই-মেইল</th>
                    <th>ঠিকানা</th>
                    <th>ফোন নং.</th>
                    <th>জাতীয় পরিচয় নং</th>
                    <th>রক্তের গ্রুপ</th>
                    <th>পিতার নাম</th>
                    <th>মাতার নাম</th>
                    <th>এডিট</th>
                    <th>পেমেন্ট</th>
                  </tr>
                  <tr>
                    <td>1</td>
                    <td>98734</td>
                    <td>565</td>
                    <td>ccc</td>
                    <td>MO</td>
                    <td>4545</td>
                    <td>11-7-2014</td>
                    <td>Active</td>
                    <td>a@mail.com</td>
                    <td>Khulna</td>
                    <td>55</td>
                    <td>1200</td>
                    <td>A+</td>
                    <td>kdsnd</td>
                    <td>fgfgg</td>
                    <td><button class="btn-outline-primary"><i class="fa fa-edit"></i></button></td>
                    <td><button class="btn-outline-success"><i class="fa fa-money"></i></button></td>
                  </tr>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>