<div class="card-body">
	<div class="card card-default ">
		<div class="card-header bg-secondary">
			<h3 class="card-title text-center">কর্মী জি এস এস হিসাব</h3>
		</div>
		<div class="text-center">
			<form role="form" >
				<div class="form-group">
					<label>
						<input type="radio" name="r1" class="minimal" checked>
						নতুন হিসাব 
					</label>
					<label>
						<input type="radio" name="r1" class="minimal">
						পুরাতন হিসাব 
					</label>
				</div>
			</form>
		</div>
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">কর্মী জি এস এস হিসাব খুলুন </h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="accountNo" >হিসাব নং</label>
								<input type="text" class="form-control" id="accountNo" placeholder="হিসাব নং">
							</div>
							<div class="form-group col-md-4">
								<label for="accType" >হিসাবের ধরন </label>
								<input type="text" class="form-control" id="accType" placeholder="হিসাবের ধরন">
							</div>
							<div class="form-group col-md-4">
								<label for="date" >খোলার তারিখ </label>
								<input type="date" class="form-control" id="date" placeholder="খোলার তারিখ">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="memberName" >সদস্যের নাম </label>
								<input type="text" class="form-control" id="memberName" placeholder="সদস্যের নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="father" >পিতার নাম </label>
								<input type="text" class="form-control" id="father" placeholder="পিতার নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="mother" >মাতার নাম </label>
								<input type="text" class="form-control" id="mother" placeholder="মাতার নাম">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="husband">স্বামী / স্ত্রীর নাম</label>
								<input type="text" class="form-control" id="husband" placeholder="স্বামী / স্ত্রীর নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="bDate" >জন্ম তারিখ </label>
								<input type="date" class="form-control" id="bDate" placeholder="Date of Birth">
							</div>
							<div class="form-group col-md-4">
								<label for="age" >বয়স </label>
								<input type="number" class="form-control" id="age" placeholder="বয়স">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-12">
								<label for="nationalID" >জাতীয় পরিচয়পত্র নং </label>
								<input type="number" class="form-control" id="nationalID" placeholder="জাতীয় পরিচয়পত্র নং">
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="preAdd" >বর্তমান ঠিকানা </label>
								<input type="text" class="form-control" id="preAdd" placeholder="বর্তমান ঠিকানা ">
							</div>
							<div class="form-group col-md-3">
								<label for="prePost" >পোস্ট অফিস </label>
								<input type="text" class="form-control" id="prePost" placeholder="পোস্ট অফিস">
							</div>
							<div class="form-group col-md-3">
								<label for="preThana" >থানা</label>
								<input type="text" class="form-control" id="preThana" placeholder="থানা">
							</div>
							<div class="form-group col-md-3">
								<label for="preDist" >জেলা</label>
								<input type="text" class="form-control" id="preDist" placeholder="জেলা">
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="perAdd" >স্থায়ী ঠিকানা</label>
								<input type="text" class="form-control" id="perAdd" placeholder="স্থায়ী ঠিকানা">
							</div>
							<div class="form-group col-md-3"> 
								<label for="perPost" >পোস্ট অফিস</label>
								<input type="text" class="form-control" id="perPost" placeholder="পোস্ট অফিস">
							</div>
							<div class="form-group col-md-3">
								<label for="perThana" >থানা</label>
								<input type="text" class="form-control" id="perThana" placeholder="থানা">
							</div>
							<div class="form-group col-md-3">
								<label for="perDist" >জেলা</label>
								<input type="text" class="form-control" id="perDist" placeholder="জেলা">
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="form-group col-md-4">
								<label for="occupation" >পেশা / সেবা </label>
								<select class="form-control" id="occupation">
									<option>নির্বাচন করুন</option>
									<option>Gov Service</option>
									<option>Private Service</option>
									<option>Business</option>
									<option>House Wife</option>
									<option>Student</option>
									<option>Others</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="mobile" >মোবাইল</label>
								<input type="text" class="form-control" id="mobile" placeholder="মোবাইল">
							</div>
							<div class="form-group col-md-4">
								<label for="mail">ই-মেইল</label>
								<input type="text" class="form-control" id="mail" placeholder="ই-মেইল">
							</div>
						</div> 
					</div>
				</div><!-- card1 end-->
				<div class="card card-default"><!-- card2 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">পরিচয়কারীর তথ্য</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="accNumber" >হিসাব নং</label>
								<input type="text" class="form-control" id="accNumber" placeholder="হিসাব নং">
							</div>
							<div class="form-group col-md-4">
								<label for="intMobile" >মোবাইল</label>
								<input type="text" class="form-control" id="intMobile" placeholder="মোবাইল">
							</div>
							<div class="form-group col-md-4">
								<label for="in_do">উন্নয়ন সম্মাননা (%):</label>
								<select class="form-control" id="in_do" name="in_do">
									<option value="0">নির্বাচন করুন</option>										
									<option value="2">2</option>
									<option value="">2.5</option>
									<option value="3">3</option>
									<option value="3.5">3.5</option>
									<option value="4">4</option>
									<option value="4.5">4.5</option>
									<option value="5">5</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
								<label for="intName" >নাম</label>
								<input type="text" class="form-control" id="intName" placeholder="নাম">
							</div>
							<div class="form-group col-md-6">
								<label for="intadd" >ঠিকানা</label>
								<input type="text" class="form-control" id="intadd" placeholder="ঠিকানা">
							</div>
						</div>

					</div>
				</div><!-- card2 end -->
				<div class="card card-default"> <!-- card3 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">নমিনির তথ্য </h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="" >নমিনির নাম</label>
								<input type="text" class="form-control" id="" placeholder="নমিনির নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="" >পিতার নাম</label>
								<input type="text" class="form-control" id="" placeholder="পিতার নাম">
							</div>
							<div class="form-group col-md-4">
								<label for="" >মাতার নাম </label>
								<input type="text" class="form-control" id="" placeholder="মাতার নাম ">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="nomineePreADD" >বর্তমান ঠিকানা</label>
								<input type="text" class="form-control" id="nomineePreADD" placeholder="বর্তমান ঠিকানা">
							</div>
							<div class="form-group col-md-3">
								<label for="nomineePrepost" >পোস্ট অফিস</label>
								<input type="text" class="form-control" id="nomineePrepost" placeholder="পোস্ট অফিস">
							</div>
							<div class="form-group col-md-3">
								<label for="nomineePrethana" >থানা</label>
								<input type="text" class="form-control" id="nomineePrethana" placeholder="থানা">
							</div>
							<div class="form-group col-md-3">
								<label for="nomineePredist" >জেলা</label>
								<input type="text" class="form-control" id="nomineePredist" placeholder="জেলা">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label for="relation" >সম্পর্ক </label>
								<select class="form-control" id="relation">
									<option value="">Select</option>
									<option value="Mother">মা</option>
									<option value="Father">বাবা</option>
									<option value="Brother">ভাই</option>
									<option value="Sister">বোন</option>
									<option value="Wife">স্ত্রী</option>
									<option value="Husband">স্বামী</option>
									<option value="Son">ছেলে</option>
									<option value="Daughter">মেয়ে</option>
									<option value="Others">অন্যান্য</option>
								</select>
							</div>
							<div class="form-group col-md-3">
								<label for="nomiBDate">জন্ম তারিখ</label>
								<input type="date" class="form-control" id="nomiBDate" placeholder="Date of Birth:">
							</div>
							<div class="form-group col-md-3">
								<label for="nm1_percent" >শতকরা:</label>
								<select class="form-control" name="nm1_percent" id="nm1_percent">
									<option value="">Select</option>
									<option value="100">100</option>
									<option value="75">75</option>
									<option value="60">60</option>
									<option value="25">25</option>
									<option value="Sheria">Sheria</option>										
								</select>
							</div>
							<div class="form-group col-md-3">
								<label for="nomiMobile" >মোবাইল</label>
								<input type="text" class="form-control" id="nomiMobile" placeholder="মোবাইল">
							</div>
						</div>
					</div>
				</div><!-- card3 end-->
				<div class="card card-default"> <!-- card4 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">ছবি এবং স্বাক্ষর</h3>

						<div class="card-tools">
							<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
							<!-- <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body pt-2">
						<div class="row">
							<div class="form-group col-md-3">
								<label for="" >সদস্যের ছবি</label>
								<input type="file" class="form-control" id="" placeholder="সদস্যের ছবি">
							</div>
							<div class="form-group col-md-3">
								<label for="" >সদস্যের স্বাক্ষর</label>
								<input type="file" class="form-control" id="" placeholder="সদস্যের স্বাক্ষর">
							</div>
							<div class="form-group col-md-3">
								<label for="" >নমিনির  ছবি</label>
								<input type="file" class="form-control" id="" placeholder="নমিনির  ছবি">
							</div>
							<div class="form-group col-md-3">
								<label for="" >নমিনির স্বাক্ষর</label>
								<input type="file" class="form-control" id="" placeholder="নমিনির স্বাক্ষর">
							</div>
						</div>
					</div>
				</div><!-- card4 end-->
			</div>
			<div class="card-body pt-0">
				<div class="row">
					<div class="form-group col-md-3">
						<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> সংরক্ষন করুন</button>
					</div>
					<div class="form-group col-md-3">
						<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-pencil"></i> আপডেট করুন</button>
					</div>
					<div class="form-group col-md-3">
						<button type="button" class="btn btn-block btn-outline-primary"><i class="fa fa-print"></i> প্রিন্ট</button>
					</div>
					<div class="form-group col-md-3">
						<button type="button" class="btn btn-block btn-outline-danger"><i class="fa fa-close"></i> বাতিল করুন</button>
					</div>
				</div>
			</div><!-- card5 end-->	
		</div>
		</form>
	</div>
</div>