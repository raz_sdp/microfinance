<div class="card">
	<div class="card-body">
		<div class="card card-default">
			<div class="card-header bg-secondary ">
				<h3 class="card-title text-center">Adjustment</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<form role="form">
					<div class="card-body">
						<div class="card card-default"> <!-- card1 -->
							  <div class="card-header bg-primary">
								<h3 class="card-title">গ্রাহকের তথ্য:</h3>
							  </div>
							  <!-- /.card-header -->

							<div class="card-body">
								<div class="row">
									<div class="form-group col-md-12">
										<label for="one">Head</label>
										<select class="form-control" id="one">
											<option value="0">নির্বাচন করুন</option>
											<option value="3">Building</option>
											<option value="4">My Capital</option>
											<option value="5">Equipment</option>
											<option value="10">Sales Income</option>
											<option value="12">Accounts Receivable</option>
											<option value="13">Accounts Payable</option>
											<option value="14">Mobile Bill</option>
											<option value="15">Prepaid Expense</option>
											<option value="16">Sales Returns and Allowances</option>
											<option value="17">Sales Discounts</option>
											<option value="18">Product Purchase</option>
											<option value="19">Purchase Return and Allowances</option>
											<option value="20">Purchase Discount</option>
											<option value="21">Drawings</option>
											<option value="22">Service Revenue</option>
											<option value="23">Salary Expense</option>
											<option value="40">MS</option>
											<option value="41">YS</option>
											<option value="42">HSS</option>
											<option value="43">FD</option>
											<option value="44">MP</option>
											<option value="45">GS</option>
											<option value="46">Accopen Fee</option>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label for="">Increase/Decrease:</label>
										<select class="form-control">
											
											<option value="increase">Increase</option>
											<option value="decrease">Decrease</option>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label for="">Amount:</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
								</div>
								<div class="form-group col-md-4 offset-4">
									<button type="button" class="btn btn-block btn-outline-primary"><i class="fa fa-money"></i> সমন্বয় করুন </button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-4">
				<form role="form">
					<div class="card-body">
						<div class="card card-default"> <!-- card1 -->
							  <!-- /.card-header -->

							<div class="card-body">
								<div class="row">
									<div class="form-group col-md-12">
										<label for="">Current Head Amount</label>
										<input type="text" class="form-control" id="" disabled>
									</div>
									
								</div>
								
								
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div><!-- card body-->
</div><!-- card -->