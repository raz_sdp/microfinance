<div class="card">
	<div class="card-body">
		<div class="card card-default">
			<div class="card-header bg-secondary ">
				<h3 class="card-title text-center">ব্যালেন্স ট্র্য্যান্সফার</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8">
				<form role="form">
					<div class="card-body">
						<div class="card card-default"> <!-- card1 -->
							  <div class="card-header bg-primary">
								<h3 class="card-title">গ্রাহকের তথ্য:</h3>
							  </div>
							  <!-- /.card-header -->

							<div class="card-body">
								<div class="row">
									<div class="form-group col-md-6">
										<label for="">তারিখ</label>
										<input type="date" class="form-control" id="" placeholder="">
									</div>

									<div class="form-group col-md-6">
										<label for="">প্রেরক</label>
										<select class="form-control">
											<option>নির্বাচন করুন</option>
											<option value="ক্যাশ">ক্যাশ</option>
											<option value="ব্যাংক">ব্যাংক</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
										<label for="">প্রাপক</label>
										<select class="form-control">
											<option>নির্বাচন করুন</option>
											<option value="ক্যাশ">ক্যাশ</option>
											<option value="ব্যাংক">ব্যাংক</option>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label for="">টাকার পরিমান</label>
										<input type="text" class="form-control" id="" placeholder="">
									</div>
									
								</div>
								<div class="row">
									
									<div class="form-group col-md-12">
										<label for="">বিবরন</label>
										<textarea class="form-control">
											
										</textarea>
									</div>
								</div>
								<div class="form-group col-md-4 offset-4">
									<button type="button" class="btn btn-block btn-outline-primary"><i class="fa fa-money"></i> হস্তান্তর করুন </button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-4">
				<form role="form">
					<div class="card-body">
						<div class="card card-default"> <!-- card1 -->
							  <!-- /.card-header -->

							<div class="card-body">
								<div class="row">
									<div class="form-group col-md-12">
										<label for="">ব্যয় করা হয় (Dr)</label>
										<input type="text" class="form-control" id="" disabled>
									</div>
									
								</div>
								<div class="row">
									<div class="form-group col-md-12">
										<label for="">আয়(Cr)</label>
										<input type="text" class="form-control" id=""disabled>
									</div>
									
								</div>
								
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
	</div><!-- card body-->
</div><!-- card -->