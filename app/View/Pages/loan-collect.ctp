<div class="card">
	<div class="card-body">
		<div class="card card-default bg-secondary">
			<div class="card-header mx-auto">
				<h3 class="card-title ">লোন লেনদেন পোষ্টিং</h3>
			</div>
		</div>

		<!-- /.card-header -->
		<!-- form start -->
		<form role="form">
			<div class="card-body">
				<div class="card card-default"> <!-- card1 -->
					<div class="card-header bg-primary">
						<h3 class="card-title">লোন লেনদেন পোষ্টিং</h3>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-4">
								<label for="accountNo">হিসাব নং.</label>
								<input type="text" class="form-control" id="accountNo" placeholder="হিসাব নং.">
							</div>
							<div class="form-group col-md-4">
								<label for="payment">পরিশোধের পরিমান</label>
								<input type="text" class="form-control" id="payment" placeholder="পরিশোধের পরিমান">
							</div>
							<div class="form-group col-md-4">
								<label for="paymentDate">পরিশোধের তারিখ</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text"><i class="fa fa-calendar"></i></span>
									</div>
									<input type="text" class="form-control" id="paymentDate" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="form-group col-md-4">
							<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-save"> </i> সংরক্ষন করুন</button>
						</div>
						</div>
						
					</div>
				</div>
			</div>
		</form>
	</div>
</div>