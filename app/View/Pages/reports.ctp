<div class="card-body">
    <div class="card card-default">
        <div class="card-header bg-secondary">
          <h3 class="card-title text-center">রিপোর্ট</h3>
      </div>

      <div class="card-body">
          <div class="card">
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">রিপোর্ট</h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item dropdown">
                      <button class=" btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false">শেয়ার রিপোর্ট</button>
                      <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                        <a class="dropdown-item" tabindex="-1" href="#shareMember" data-toggle="tab">শেয়ার সদস্য</a>
                        <a class="dropdown-item" tabindex="-1" href="#totalReports" data-toggle="tab">বিস্তারিত প্রতিবেদন</a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                      <button class=" btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false">সঞ্চয় রিপোর্ট
                      </button>
                  </a>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <a class="dropdown-item" tabindex="-1" href="#generalSaving" data-toggle="tab">সাধারণ সঞ্চয়</a>
                    <a class="dropdown-item" tabindex="-1" href="#weekSavign" data-toggle="tab">সাপ্তাহিক সঞ্চয়</a>
                    <a class="dropdown-item" tabindex="-1" href="#monthlySaving" data-toggle="tab">মাসিক সঞ্চয়</a>
                    <a class="dropdown-item" tabindex="-1" href="#yearlySaving" data-toggle="tab">বাৎসরিক সঞ্চয়</a>
                    <a class="dropdown-item" tabindex="-1" href="#onetimeSaving" data-toggle="tab">এক কালীন সঞ্চয়</a>
                    <a class="dropdown-item" tabindex="-1" href="#monthlyBenefit" data-toggle="tab">মাসিক লভাংশ্য</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <button class=" btn btn-primary dropdown-toggle mr-1" data-toggle="dropdown" aria-expanded="false">লোন রিপোর্ট
                </button>
              </a>
              <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;">
                <a class="dropdown-item" tabindex="-1" href="#loanMembers" data-toggle="tab">লোন সদস্য </a>
                <a class="dropdown-item" tabindex="-1" href="#groupMem" data-toggle="tab">গ্রপ ভিত্তিক আদায়</a>
            </div>
        </li>
    </ul>
</div><!-- /.card-header -->
<div class="card-body">
    <div class="tab-content">
        <div class="tab-pane active show" id="shareMember">
            Share Member
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="totalReports">
            Total Reports
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="generalSaving">
            নির্মাণাধীন..........
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="weekSavign">
            নির্মাণাধীন..........
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="monthlySaving">
            নির্মাণাধীন..........
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="yearlySaving">
            নির্মাণাধীন..........
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="onetimeSaving">
            নির্মাণাধীন.
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="monthlyBenefit">
            নির্মাণাধীন....
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="shareMember">
            Share Member
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="loanMembers">
                loanmembers
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="shareMember">
            Share Member
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="groupMem">
            Group
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div><!-- /.card-body -->
</div>
</div>
</div>
</div>


