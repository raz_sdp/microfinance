<div class="card-body">
	<div class="card card-default">
		<div class="card-header bg-secondary">
			<div class="row">
				<h3 class="col-md-8 card-title">উত্তোলন পোষ্টিং</h3>
				<form class="col-md-4 form-inline">
					<div class="input-group input-group-sm">
						<input class="form-control form-control-navbar" type="search" placeholder="লেনদেনের জন্য খুজুন...." aria-label="Search">
						<div class="input-group-append">
							<button class="btn btn-navbar" type="submit">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>	
			</div>
		</div>
		<div class="container-fluid mt-2">
			<div class="row">
				<!-- left column -->
				<div class="col-md-6">
					<!-- general form elements -->
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">পোষ্টিং</h3>
							<div class="card-tools">
								<p>বর্তমান ব্যালান্স 00</p> 
							</div>
						</div>
						<!-- /.card4header -->
						<!-- form start -->
						<form role="form">
							<div class="card-body">
								<div class="row">						
									<div class="form-group col-md-4">
										<label for="">হিসাব নং.</label>
										<input type="text" class="form-control" id="" placeholder="হিসাব নং.">
									</div>
									<div class="form-group col-md-4">
										<label for="">পরিশোধের পরিমান</label>
										<input type="text" class="form-control" id="" placeholder="পরিমান">
									</div>
									<div class="form-group col-md-4">
										<label for="">সিশ্চিত পরিমান</label>
										<input type="text" class="form-control" id="" placeholder="সিশ্চিত পরিমান">
									</div>
									<div class="form-group col-md-5">
										<label for="">পরিশোধের তারিখ</label>
										<input type="text" class="form-control" id="" placeholder="পরিশোধের তারিখ">
									</div>
									<div class="form-group col-md-7">
										<label for="">বিবরন.</label>
										<input type="text" class="form-control" id="" placeholder="বিবরন">
									</div>

									<div class="form-group col-md-4">
										<button type="button" class="btn btn-block btn-outline-success"><i class="fa fa-save"></i> সংরক্ষন করুন</button>
									</div>
									<div class="form-group col-md-4">
										<button type="button" class="btn btn-block btn-outline-info"><i class="fa fa-refresh"></i> রিফ্রেশ করুন</button>
									</div>
									<div class="form-group col-md-4">
										<button type="button" class="btn btn-block btn-outline-danger"><i class="fa fa-undo"></i> বের হয়ে আসুন</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- /.row -->
				<!-- right column -->
				<div class="col-md-6">
					<!-- Horizontal Form -->
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">হিসাবের তথ্য</h3>
						</div>
						<!-- /.card-header -->
						<!-- form start -->
						<form class="form-horizontal">
							<div class="card-body">
								<div class="row">						
									<div class="form-group col-md-6">
										<label for="">গ্রাহকের নাম</label>
										<input type="text" class="form-control" id="" placeholder="গ্রাহকের নাম" disabled>
									</div>
									<div class="form-group col-md-6">
										<label for="">হিসাবের ধরন</label>
										<input type="text" class="form-control" id="" placeholder="হিসাবের ধরন" disabled>
									</div>
									<div class="form-group col-md-6">
										<label for="">পরিশোধের ধরন</label>
										<input type="text" class="form-control" id="" placeholder="পরিশোধের ধরন" disabled>
									</div>
									<div class="form-group col-md-6">
										<label for="">ব্যালান্স	</label>
										<input type="text" class="form-control" id="" placeholder="ব্যালান্স	" disabled>
									</div>
									
									<div class="pb-3 form-group col-md-6 offset-1">
										<input type="checkbox" class="form-check-input">
										<label class="form-check-label" for="exampleCheck2">কার্ডের ছবি</label>
									</div>
								</div>
							</div>	
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="container pt-2">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header bg-primary">
							<h3 class="card-title">উত্তোলনকারী সদস্যদের তালিকা</h3>

							<div class="card-tools">
								<div class="input-group input-group-sm" style="width: 150px;">
									<input type="text" name="table_search" class="form-control float-right" placeholder="Search">

									<div class="input-group-append">
										<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
									</div>
								</div>
							</div>
						</div>
						<!-- /.card-header -->
						<div class="card-body table-responsive p-0">
							<table class="table table-hover small">
								<tr>
									<th>ক্রঃ নং</th>
									<th>এম আর নং</th>
									<th>শাখার নাম</th>
									<th>হিসাব নং</th>
									<th>তারিখ</th>
									<th>উত্তোলনের পরিমান</th>
									<th>বর্তমান টাকা</th>
									<th>বিবরন</th>
									<th>এডিট </th>
									<th>প্রিন্ট</th>
								</tr>
								<tr>
									<td>1</td>
									<td>98734</td>
									<td>Khulna</td>
									<td>1234567</td>
									<td>11-7-2014</td>
									<td>1357</td>
									<td>100000</td>
									<td>asdf</td>
									<td><i class="fa fa-pencil"></i> Edit</td>
									
									<td><i class="fa fa-print"></i> Print</td>
								</tr>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</div>
</div>