  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2018 <a href="">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b>
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <?php
  echo $this->Html->script([
    'plugins/jquery/jquery.min',
    'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js',    
  ]);
  ?>

<script type="text/javascript">
   $.widget.bridge('uibutton', $.ui.button)
</script>
  <?php
  echo $this->Html->script([
    'plugins/bootstrap/js/bootstrap.bundle.min',
    'https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',
    'plugins/morris/morris.min',
    'plugins/sparkline/jquery.sparkline.min',
    'plugins/jvectormap/jquery-jvectormap-1.2.2.min',
    'plugins/jvectormap/jquery-jvectormap-world-mill-en',
    'plugins/knob/jquery.knob',
    'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js',
    'plugins/daterangepicker/daterangepicker',
    'plugins/daterangepicker/daterangepicker',
    'plugins/datepicker/bootstrap-datepicker',
    'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all',
    'plugins/slimScroll/jquery.slimscroll.min',
    'plugins/fastclick/fastclick',
    'dist/js/adminlte',
    'dist/js/pages/dashboard',
    'dist/js/demo',
    'custom',
    
  ]);
  ?>

