	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<!-- Brand Logo -->
		<a href="<?php echo $this->Html->url('/dashboard',true);?>" class="brand-link">
			<img src="<?php echo $this->Html->url('/dist/img/images.png',true);?>" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
			<span class="brand-text font-weight-light">সমবায় সমিতি লিঃ</span>
		</a>

		<!-- Sidebar -->
		<div class="sidebar">
			<!-- Sidebar user panel (optional) -->
			<div class="user-panel mt-3 pb-3 mb-3 d-flex">
				<div class="image">
					<img src="<?php echo $this->Html->url('/dist/img/user2-160x160.jpg',true);?>" class="img-circle elevation-2" alt="User Image">
				</div>
				<div class="info">
					<a href="#" class="d-block">Super Admin</a>
				</div>
			</div>

			<!-- Sidebar Menu -->
			<nav class="mt-2">
				<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
					<!-- Add icons to the links using the .nav-icon class
							 with font-awesome or any other icon font library -->
					<li class="nav-item has-treeview menu-open">
						<a href="<?php echo $this->Html->url('/dashboard',true);?>" class="nav-link active">
							<i class="nav-icon fa fa-dashboard"></i>
							<p>
								হোম          
							</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="<?php echo $this->Html->url('/admin/members/index/1') ?>" class="nav-link">
							<i class="nav-icon fa fa-th"></i>
							<p>
								সদস্যসের তালিকা
								
							</p>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<a href="#" class="nav-link">
							<i class="nav-icon fa fa-pie-chart"></i>
							<p>
								সঞ্চয়ী হিসাব

								<i class="right fa fa-angle-left"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="<?php echo $this->Html->url('/admin/members/add') ?>" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>আবেদন ফরম</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="saving-collect" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>সঞ্চয় আদায়</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="diposit_withdrawal" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>সঞ্চয় বা লভ্যাংশ উত্তোলন</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="saving_acc_closing" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>হিসাব বন্ধ করুন</p>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item has-treeview">
						<a href="#" class="nav-link">
							<i class="nav-icon fa fa-tree"></i>
							<p>
								লোন হিসাব
								<i class="fa fa-angle-left right"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="loan_acc_open" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p> লোন আবেদন ফরম</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="loan-collect" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>কিস্তি আদায়</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="loan-acc-close" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>লোন হিসাব বন্ধ করুন</p>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item has-treeview">
						<a href="#" class="nav-link">
							<i class="nav-icon fa fa-edit"></i>
							<p>
								কর্মচারীগন
								<i class="fa fa-angle-left right"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="staff" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>কর্মী এন্ট্রি এবং পেমেন্ট</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="staff-acc-open" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>কর্মী জি এস এস এন্ট্রি ফরম</p>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item has-treeview">
						<a href="#" class="nav-link">
							<i class="nav-icon fa fa-table"></i>
							<p>
								সাধারন হিসাব
								<i class="fa fa-angle-left right"></i>
							</p>
						</a>
						<ul class="nav nav-treeview">
							<li class="nav-item">
								<a href="income" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>আয়</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="payment" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>ব্যায়</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="transfer" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>নগদ স্থানান্তর</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="adjustment" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>হেড সমন্বয়</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="bank-reconciliation" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>ব্যাংক রিকনসুলেশন</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="acc-head" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>হেড যোগ করুন</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="ledger-chart" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>খতিয়ান</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="bank-acc-open" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>ব্যাংক হিসাব</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="acc-chart" class="nav-link">
									<i class="fa fa-circle-o nav-icon"></i>
									<p>হিসাবের তালিকা</p>
								</a>
							</li>
						</ul>
					</li>
					<li class="nav-item">
						<a href="#" class="nav-link">
							<i class="nav-icon fa fa-calendar"></i>
							<p>
								 চালান এডিট
							</p>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<a href="profit-payment" class="nav-link">
							<i class="nav-icon fa fa-envelope-o"></i>
							<p>
								 লভ্যাংশ ব্যবস্থাপনা
							</p>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<a href="reports" class="nav-link">
							<i class="nav-icon fa fa-book"></i>
							<p>
								 রিপোর্ট
							</p>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<a href="settings" class="nav-link">
							<i class="nav-icon fa fa-cog"></i>
							<p>
								সেটিংস
							</p>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<a href="<?php echo $this->Html->url('/admin/users/logout',true) ?>" class="nav-link">
							<i class="nav-icon fa fa-power-off"></i>
							<p>লগ আউট</p>
						</a>
					</li>
					
			</nav>
			<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
	</aside>