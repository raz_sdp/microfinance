<div class="accounts form">
<?php echo $this->Form->create('Account'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Account'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('member_id');
		echo $this->Form->input('accounttype_id');
		echo $this->Form->input('accNo');
		echo $this->Form->input('accCreatedAt');
		echo $this->Form->input('period');
		echo $this->Form->input('storageAmmount');
		echo $this->Form->input('totlaAmmount');
		echo $this->Form->input('rcvProfit');
		echo $this->Form->input('depositAmmount');
		echo $this->Form->input('expiredDate');
		echo $this->Form->input('admissionFee');
		echo $this->Form->input('withdraw');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Account.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Account.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Accounts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Members'), array('controller' => 'members', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member'), array('controller' => 'members', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('controller' => 'accounttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('controller' => 'accounttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
