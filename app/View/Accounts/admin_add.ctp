<div class="card-body">
	<div class="card card-default">
		<div class="card-header bg-secondary">
			<h3 class="card-title text-center">সঞ্চয়ী হিসাব খুলুন</h3>
		</div>
		<div class="card-body">
			<div class="card-body">
				<div class="row">
					<div class="form-group col-md-4">
						<label for="accType">হিসাবের ধরন</label>
						<select class="form-control" id="accType" name="accType">
						    <option value="0">হিসাবের ধরন</option>
						</select>
					</div>
					<div class="form-group col-md-4">
						<label for="">হিসাব নং</label>
						<input type="text" class="form-control" id="" placeholder="হিসাব নং">
					</div>
					<div class="form-group col-md-4">
						<label for="">খোলার তারিখ</label>
						<input type="date" class="form-control" id="" placeholder="খোলার তারিখ">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-4">
						<label for="">মেয়াদ</label>
						<input type="text" class="form-control" id="" placeholder="মেয়াদ">
					</div>
					<div class="form-group col-md-4">
						<label for="">সঞ্চয়ের পরিমান</label>
						<input type="text" class="form-control" id="" placeholder="সঞ্চয়ের পরিমান">
					</div>
					<div class="form-group col-md-4">
						<label for="">মোট জমা</label>
						<input type="text" class="form-control" id="" placeholder="মোট জমা">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-4">
						<label for="">মুনাফা সহ সম্ভাব্য প্রাপ্য</label>
						<input type="text" class="form-control" id="" placeholder="মুনাফা সহ সম্ভাব্য প্রাপ্য">
					</div>
					<div class="form-group col-md-4">
						<label for="">জমাকৃত টাকা</label>
						<input type="text" class="form-control" id="" placeholder="জমাকৃত টাকা">
					</div>
					<div class="form-group col-md-4">
						<label for="">মেয়াদপূর্তির তারিখ</label>
						<input type="date" class="form-control" id="" placeholder="মেয়াদপূর্তির তারিখ">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-4">
						<label for="">ভর্তি ফি</label>
						<input type="text" class="form-control" id="" placeholder="ভর্তি ফি">
					</div>
					<div class="form-group col-md-4">
						<label for="">উত্তোলন</label>
						<input type="number" class="form-control" id="" placeholder="উত্তোলন">
					</div>
				</div>			
			</div>
			<button type="submit" class="mt-2 btn btn-block btn-outline-primary col-md-4 offset-4">সঞ্চয়ী হিসাব খুলুন</button>							

		</div>
	</div>
</div>

<!-- <div class="accounts form">
<?php echo $this->Form->create('Account'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Account'); ?></legend>
	<?php
		echo $this->Form->input('member_id');
		echo $this->Form->input('accounttype_id');
		echo $this->Form->input('accNo');
		echo $this->Form->input('accCreatedAt');
		echo $this->Form->input('period');
		echo $this->Form->input('storageAmmount');
		echo $this->Form->input('totlaAmmount');
		echo $this->Form->input('rcvProfit');
		echo $this->Form->input('depositAmmount');
		echo $this->Form->input('expiredDate');
		echo $this->Form->input('admissionFee');
		echo $this->Form->input('withdraw');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Accounts'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Members'), array('controller' => 'members', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member'), array('controller' => 'members', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('controller' => 'accounttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('controller' => 'accounttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
 -->