<div class="accounts view">
<h2><?php echo __('Account'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($account['Account']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Member'); ?></dt>
		<dd>
			<?php echo $this->Html->link($account['Member']['id'], array('controller' => 'members', 'action' => 'view', $account['Member']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accounttype'); ?></dt>
		<dd>
			<?php echo $this->Html->link($account['Accounttype']['id'], array('controller' => 'accounttypes', 'action' => 'view', $account['Accounttype']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($account['Account']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AccNo'); ?></dt>
		<dd>
			<?php echo h($account['Account']['accNo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AccCreatedAt'); ?></dt>
		<dd>
			<?php echo h($account['Account']['accCreatedAt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Period'); ?></dt>
		<dd>
			<?php echo h($account['Account']['period']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('StorageAmmount'); ?></dt>
		<dd>
			<?php echo h($account['Account']['storageAmmount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('TotlaAmmount'); ?></dt>
		<dd>
			<?php echo h($account['Account']['totlaAmmount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('RcvProfit'); ?></dt>
		<dd>
			<?php echo h($account['Account']['rcvProfit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('DepositAmmount'); ?></dt>
		<dd>
			<?php echo h($account['Account']['depositAmmount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('ExpiredDate'); ?></dt>
		<dd>
			<?php echo h($account['Account']['expiredDate']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('AdmissionFee'); ?></dt>
		<dd>
			<?php echo h($account['Account']['admissionFee']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Withdraw'); ?></dt>
		<dd>
			<?php echo h($account['Account']['withdraw']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($account['Account']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Account'), array('action' => 'edit', $account['Account']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Account'), array('action' => 'delete', $account['Account']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $account['Account']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounts'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Account'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Members'), array('controller' => 'members', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member'), array('controller' => 'members', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('controller' => 'accounttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('controller' => 'accounttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
