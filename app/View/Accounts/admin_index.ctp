<div class="accounts index">
	<h2><?php echo __('Accounts'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('member_id'); ?></th>
			<th><?php echo $this->Paginator->sort('accounttype_id'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('accNo'); ?></th>
			<th><?php echo $this->Paginator->sort('accCreatedAt'); ?></th>
			<th><?php echo $this->Paginator->sort('period'); ?></th>
			<th><?php echo $this->Paginator->sort('storageAmmount'); ?></th>
			<th><?php echo $this->Paginator->sort('totlaAmmount'); ?></th>
			<th><?php echo $this->Paginator->sort('rcvProfit'); ?></th>
			<th><?php echo $this->Paginator->sort('depositAmmount'); ?></th>
			<th><?php echo $this->Paginator->sort('expiredDate'); ?></th>
			<th><?php echo $this->Paginator->sort('admissionFee'); ?></th>
			<th><?php echo $this->Paginator->sort('withdraw'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($accounts as $account): ?>
	<tr>
		<td><?php echo h($account['Account']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($account['Member']['id'], array('controller' => 'members', 'action' => 'view', $account['Member']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($account['Accounttype']['id'], array('controller' => 'accounttypes', 'action' => 'view', $account['Accounttype']['id'])); ?>
		</td>
		<td><?php echo h($account['Account']['modified']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['accNo']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['accCreatedAt']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['period']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['storageAmmount']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['totlaAmmount']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['rcvProfit']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['depositAmmount']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['expiredDate']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['admissionFee']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['withdraw']); ?>&nbsp;</td>
		<td><?php echo h($account['Account']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $account['Account']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $account['Account']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $account['Account']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $account['Account']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Account'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Members'), array('controller' => 'members', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Member'), array('controller' => 'members', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Accounttypes'), array('controller' => 'accounttypes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Accounttype'), array('controller' => 'accounttypes', 'action' => 'add')); ?> </li>
	</ul>
</div>
