-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2019 at 08:59 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `microfinance`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `member_id` int(11) DEFAULT NULL,
  `accounttype_id` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `accNo` varchar(0) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accCreatedAt` datetime DEFAULT NULL,
  `period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storageAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totlaAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rcvProfit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depositAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiredDate` datetime DEFAULT NULL,
  `admissionFee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `withdraw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `member_id`, `accounttype_id`, `modified`, `accNo`, `accCreatedAt`, `period`, `storageAmmount`, `totlaAmmount`, `rcvProfit`, `depositAmmount`, `expiredDate`, `admissionFee`, `withdraw`, `created`) VALUES
(1, 4, NULL, '2019-05-20 10:39:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 10:39:00'),
(2, 5, NULL, '2019-05-20 10:39:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 10:39:15'),
(3, 6, NULL, '2019-05-20 10:39:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-20 10:39:56'),
(4, 7, NULL, '2019-05-21 08:15:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-21 08:15:09');

-- --------------------------------------------------------

--
-- Table structure for table `accounttypes`
--

CREATE TABLE `accounttypes` (
  `id` int(11) NOT NULL,
  `accountType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `accounttypes`
--

INSERT INTO `accounttypes` (`id`, `accountType`, `created`, `modified`) VALUES
(1, 'gs', '2019-05-14 11:07:56', '2019-05-14 11:07:56'),
(2, 'ds', '2019-05-14 11:08:02', '2019-05-14 11:08:02'),
(3, 'pd', '2019-05-14 11:08:08', '2019-05-14 11:08:08'),
(4, 'psp', '2019-05-14 11:08:19', '2019-05-14 11:08:19');

-- --------------------------------------------------------

--
-- Table structure for table `addaccountheds`
--

CREATE TABLE `addaccountheds` (
  `id` int(11) NOT NULL,
  `accountName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `adjustments`
--

CREATE TABLE `adjustments` (
  `id` int(11) NOT NULL,
  `head` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentHeadAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `closeloanaccounts`
--

CREATE TABLE `closeloanaccounts` (
  `id` int(11) NOT NULL,
  `mrNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acoountType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Installment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closingBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `closesavingaccounts`
--

CREATE TABLE `closesavingaccounts` (
  `id` int(11) NOT NULL,
  `accNo` text COLLATE utf8_unicode_ci,
  `accType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymenType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closingBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totalAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `collectedloans`
--

CREATE TABLE `collectedloans` (
  `id` int(11) NOT NULL,
  `accountId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentDate` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `designation`, `created`, `modified`) VALUES
(1, 'mo', '2019-05-13 11:06:03', '2019-05-13 11:06:03');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `grpCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grpName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `president` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `village` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `grpCode`, `grpName`, `president`, `village`, `created`, `modified`) VALUES
(1, '1223', 'asdf', 'mohsin', 'dacope', '2019-05-13 11:05:38', '2019-05-13 11:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentBalanceDr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receivedPaymentCr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ledgers`
--

CREATE TABLE `ledgers` (
  `id` int(11) NOT NULL,
  `accountHeadName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ledgerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startingBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `memberName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberFatherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberMotherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalIDNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prePost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomFather` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMother` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPreAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomBirthDate` date DEFAULT NULL,
  `pertnerPercent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomSign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `staff_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `accounttype_id` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `memberName`, `memberFatherName`, `memberMotherName`, `birthDate`, `age`, `nationalIDNo`, `occupation`, `mobileNo`, `email`, `preAddress`, `prePost`, `preDistrict`, `preThana`, `perAddress`, `perPost`, `perDistrict`, `perThana`, `nomName`, `nomFather`, `nomMother`, `nomPreAddress`, `nomPost`, `nomThana`, `nomDistrict`, `relation`, `nomBirthDate`, `pertnerPercent`, `nomMobile`, `memberPhoto`, `cardPhoto`, `nomPhoto`, `nomSign`, `staff_id`, `account_id`, `accounttype_id`, `modified`, `created`) VALUES
(1, 'wrer', 'rtrrtr', 'tettt', '0000-00-00', 'ttre', 'rrtr', 'à¦¬à§‡à¦¸à¦°à¦•à¦¾à¦°à§€ à¦šà¦¾à¦•à§à¦°à§€', 'ttrtrt', 'tttt', 'wewe', 'ewe', 'ewwer', 'ewew', 'ewrwe', 'wewe', 'ewwerw', 'ewrwrw', 'wew', 'ree', 'ffff', 'ererf', 'dfdfd', 'dfdf', 'fddf', 'dffds', '0000-00-00', 'dffdf', 'dffd', NULL, NULL, NULL, NULL, '#2#3#121#', NULL, NULL, '2019-05-14 11:03:39', '2019-05-14 11:03:39'),
(2, 'wrer', 'rtrrtr', 'tettt', '0000-00-00', 'ttre', 'rrtr', 'à¦¬à§‡à¦¸à¦°à¦•à¦¾à¦°à§€ à¦šà¦¾à¦•à§à¦°à§€', 'ttrtrt', 'tttt', 'wewe', 'ewe', 'ewwer', 'ewew', 'ewrwe', 'wewe', 'ewwerw', 'ewrwrw', 'wew', 'ree', 'ffff', 'ererf', 'dfdfd', 'dfdf', 'fddf', 'dffds', '0000-00-00', 'dffdf', 'dffd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-14 11:04:00', '2019-05-14 11:04:00'),
(3, 'wrer', 'rtrrtr', 'tettt', '0000-00-00', 'ttre', 'rrtr', 'à¦¬à§‡à¦¸à¦°à¦•à¦¾à¦°à§€ à¦šà¦¾à¦•à§à¦°à§€', 'ttrtrt', 'tttt', 'wewe', 'ewe', 'ewwer', 'ewew', 'ewrwe', 'wewe', 'ewwerw', 'ewrwrw', 'wew', 'ree', 'ffff', 'ererf', 'dfdfd', 'dfdf', 'fddf', 'dffds', '0000-00-00', 'dffdf', 'dffd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-14 11:04:28', '2019-05-14 11:04:28');

-- --------------------------------------------------------

--
-- Table structure for table `openbankaccounts`
--

CREATE TABLE `openbankaccounts` (
  `id` int(11) NOT NULL,
  `bankName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mainBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `openloanaccounts`
--

CREATE TABLE `openloanaccounts` (
  `id` int(11) NOT NULL,
  `clientName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `fatherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalidNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prePost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupassion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loanIdNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loanNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loanAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit_10` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceCharge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstGuarantorId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondGuarantorId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientImage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientSignature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `openstaffaccounts`
--

CREATE TABLE `openstaffaccounts` (
  `id` int(11) NOT NULL,
  `accNo` text COLLATE utf8_unicode_ci,
  `accType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accOpenDate` datetime DEFAULT NULL,
  `memberName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fatherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `husbandWifeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationalIDNo` text COLLATE utf8_unicode_ci,
  `preAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prePost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preDistrisct` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` text COLLATE utf8_unicode_ci,
  `email` text COLLATE utf8_unicode_ci,
  `introducerAccNo` text COLLATE utf8_unicode_ci,
  `introducerMobile` text COLLATE utf8_unicode_ci,
  `introducerReputation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introducerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introducerAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomFather` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMother` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPreAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomRelation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomBirthDate` date DEFAULT NULL,
  `percent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMobile` text COLLATE utf8_unicode_ci,
  `momberPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberSign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomSign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `income` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expencesCr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentBalanceDr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profitpayments`
--

CREATE TABLE `profitpayments` (
  `id` int(11) NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acctype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `peymentPeriod` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percentage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalIDNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bloodGrp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `joiningDate` datetime DEFAULT NULL,
  `moCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdmeCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dmeCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`id`, `name`, `address`, `phone`, `email`, `nationalIDNo`, `bloodGrp`, `father`, `mother`, `salary`, `joiningDate`, `moCode`, `pdmeCode`, `dmeCode`, `designation_id`, `group_id`, `created`, `modified`) VALUES
(1, 'asdf', 'aaa', '123579', 'asdh@gmail.com', 'tyyy555', '445', '4444', 'ffhg', '4444.00', '2019-05-14 12:02:00', '3344', NULL, NULL, 1, NULL, '2019-05-14 12:03:32', '2019-05-14 12:03:32'),
(2, 'saasa', 'ssssassa', '1223', 'sdshgdf@gmail.com', 'czxbczn', 'cbhjc', 'sdjhc', 'chsdc', '3323.00', '2019-05-14 12:19:00', '', '2233', NULL, 1, NULL, '2019-05-14 12:20:34', '2019-05-14 12:20:34'),
(3, 'sda', 'sdas', '342', 'c', 'sd', 'sd', 'fdf', 'dsf', '433434.00', '2019-05-21 15:35:34', NULL, NULL, '23232', 2, NULL, '2019-05-21 15:35:43', '2019-05-21 15:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `transfers`
--

CREATE TABLE `transfers` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expencesDr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `earningsCr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `created`, `modified`) VALUES
(1, 'admin@gmail.com', 'c9eba798d5dcbc0381d8cecd37be72c8fe615b31', '2019-05-20 12:33:30', '2019-05-20 12:33:33');

-- --------------------------------------------------------

--
-- Table structure for table `withdrawpostings`
--

CREATE TABLE `withdrawpostings` (
  `id` int(11) NOT NULL,
  `accNo` text COLLATE utf8_unicode_ci,
  `paymentAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sureAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payemntDate` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `memberName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounttypes`
--
ALTER TABLE `accounttypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addaccountheds`
--
ALTER TABLE `addaccountheds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adjustments`
--
ALTER TABLE `adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `closeloanaccounts`
--
ALTER TABLE `closeloanaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `closesavingaccounts`
--
ALTER TABLE `closesavingaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `collectedloans`
--
ALTER TABLE `collectedloans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ledgers`
--
ALTER TABLE `ledgers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `openbankaccounts`
--
ALTER TABLE `openbankaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `openloanaccounts`
--
ALTER TABLE `openloanaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `openstaffaccounts`
--
ALTER TABLE `openstaffaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profitpayments`
--
ALTER TABLE `profitpayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfers`
--
ALTER TABLE `transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `withdrawpostings`
--
ALTER TABLE `withdrawpostings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `accounttypes`
--
ALTER TABLE `accounttypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `addaccountheds`
--
ALTER TABLE `addaccountheds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adjustments`
--
ALTER TABLE `adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `closeloanaccounts`
--
ALTER TABLE `closeloanaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `closesavingaccounts`
--
ALTER TABLE `closesavingaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `collectedloans`
--
ALTER TABLE `collectedloans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledgers`
--
ALTER TABLE `ledgers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `openbankaccounts`
--
ALTER TABLE `openbankaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `openloanaccounts`
--
ALTER TABLE `openloanaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `openstaffaccounts`
--
ALTER TABLE `openstaffaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profitpayments`
--
ALTER TABLE `profitpayments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staffs`
--
ALTER TABLE `staffs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transfers`
--
ALTER TABLE `transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `withdrawpostings`
--
ALTER TABLE `withdrawpostings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
