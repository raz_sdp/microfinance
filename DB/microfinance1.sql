/*
Navicat MySQL Data Transfer

Source Server         : newCon
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : microfinance

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-06-13 16:50:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `accounttype_id` int(11) DEFAULT NULL,
  `accNo` varchar(0) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accCreatedAt` datetime DEFAULT NULL,
  `period` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `storageAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totlaAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rcvProfit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depositAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiredDate` datetime DEFAULT NULL,
  `admissionFee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `withdraw` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('1', '12', null, '', null, '', '', '', '', '', null, '', '', '2019-06-12 07:36:39', '2019-06-12 07:36:39');
INSERT INTO `accounts` VALUES ('2', '13', null, '', null, '', '', '', '', '', null, '', '', '2019-06-13 12:08:23', '2019-06-13 12:08:23');
INSERT INTO `accounts` VALUES ('3', '14', null, '', null, '', '', '', '', '', null, '', '', '2019-06-13 12:17:17', '2019-06-13 12:17:18');

-- ----------------------------
-- Table structure for `accounttypes`
-- ----------------------------
DROP TABLE IF EXISTS `accounttypes`;
CREATE TABLE `accounttypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of accounttypes
-- ----------------------------
INSERT INTO `accounttypes` VALUES ('1', 'gs', 'সাধারণ সঞ্চয়ী হিসাব', '2019-05-14 11:07:56', '2019-05-14 11:07:56');
INSERT INTO `accounttypes` VALUES ('2', 'ds', 'দৈনিক সঞ্চয়ী হিসাব', '2019-05-14 11:08:02', '2019-05-14 11:08:02');
INSERT INTO `accounttypes` VALUES ('3', 'ys', 'বাৎসরিক সঞ্চয়ী হিসাব', '2019-05-14 11:08:08', '2019-05-14 11:08:08');
INSERT INTO `accounttypes` VALUES ('4', 'psp', 'পি এস পি', '2019-05-14 11:08:19', '2019-05-14 11:08:19');
INSERT INTO `accounttypes` VALUES ('5', 'ms', 'মাসিক সঞ্চয়ী হিসাব', '2019-05-28 11:51:20', '2019-05-28 11:51:23');
INSERT INTO `accounttypes` VALUES ('6', 'ps', 'পি এস', '2019-05-28 11:52:13', '2019-05-28 11:52:19');
INSERT INTO `accounttypes` VALUES ('7', 'loan', 'লোন', '2019-05-28 11:54:44', '2019-05-28 11:54:46');

-- ----------------------------
-- Table structure for `addaccountheds`
-- ----------------------------
DROP TABLE IF EXISTS `addaccountheds`;
CREATE TABLE `addaccountheds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of addaccountheds
-- ----------------------------

-- ----------------------------
-- Table structure for `adjustments`
-- ----------------------------
DROP TABLE IF EXISTS `adjustments`;
CREATE TABLE `adjustments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `head` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentHeadAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of adjustments
-- ----------------------------

-- ----------------------------
-- Table structure for `closeloanaccounts`
-- ----------------------------
DROP TABLE IF EXISTS `closeloanaccounts`;
CREATE TABLE `closeloanaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mrNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acoountType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Installment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closingBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of closeloanaccounts
-- ----------------------------

-- ----------------------------
-- Table structure for `closesavingaccounts`
-- ----------------------------
DROP TABLE IF EXISTS `closesavingaccounts`;
CREATE TABLE `closesavingaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accNo` text COLLATE utf8_unicode_ci,
  `accType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymenType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closingBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `minus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totalAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of closesavingaccounts
-- ----------------------------

-- ----------------------------
-- Table structure for `collectedloans`
-- ----------------------------
DROP TABLE IF EXISTS `collectedloans`;
CREATE TABLE `collectedloans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentDate` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of collectedloans
-- ----------------------------

-- ----------------------------
-- Table structure for `designations`
-- ----------------------------
DROP TABLE IF EXISTS `designations`;
CREATE TABLE `designations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `designation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of designations
-- ----------------------------
INSERT INTO `designations` VALUES ('1', 'mo', '2019-05-13 11:06:03', '2019-05-13 11:06:03');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grpCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grpName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `president` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `village` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', '1223', 'asdf', 'mohsin', 'dacope', '2019-05-13 11:05:38', '2019-05-13 11:05:00');

-- ----------------------------
-- Table structure for `incomes`
-- ----------------------------
DROP TABLE IF EXISTS `incomes`;
CREATE TABLE `incomes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentBalanceDr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receivedPaymentCr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of incomes
-- ----------------------------

-- ----------------------------
-- Table structure for `ledgers`
-- ----------------------------
DROP TABLE IF EXISTS `ledgers`;
CREATE TABLE `ledgers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountHeadName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ledgerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startingBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of ledgers
-- ----------------------------

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberFatherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberMotherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalIDNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prePost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomFather` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMother` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPreAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomBirthDate` date DEFAULT NULL,
  `pertnerPercent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cardPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomSign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `staff_id` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  `accounttype_id` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('1', 'wrer', 'rtrrtr', 'tettt', '0000-00-00', 'ttre', 'rrtr', 'à¦¬à§‡à¦¸à¦°à¦•à¦¾à¦°à§€ à¦šà¦¾à¦•à§à¦°à§€', 'ttrtrt', 'tttt', 'wewe', 'ewe', 'ewwer', 'ewew', 'ewrwe', 'wewe', 'ewwerw', 'ewrwrw', 'wew', 'ree', 'ffff', 'ererf', 'dfdfd', 'dfdf', 'fddf', 'dffds', '0000-00-00', 'dffdf', 'dffd', null, null, null, null, '#2#3#121#', null, null, '2019-05-14 11:03:39', '2019-05-14 11:03:39');
INSERT INTO `members` VALUES ('2', 'wrer', 'rtrrtr', 'tettt', '0000-00-00', 'ttre', 'rrtr', 'à¦¬à§‡à¦¸à¦°à¦•à¦¾à¦°à§€ à¦šà¦¾à¦•à§à¦°à§€', 'ttrtrt', 'tttt', 'wewe', 'ewe', 'ewwer', 'ewew', 'ewrwe', 'wewe', 'ewwerw', 'ewrwrw', 'wew', 'ree', 'ffff', 'ererf', 'dfdfd', 'dfdf', 'fddf', 'dffds', '0000-00-00', 'dffdf', 'dffd', null, null, null, null, null, null, null, '2019-05-14 11:04:00', '2019-05-14 11:04:00');
INSERT INTO `members` VALUES ('3', 'wrer', 'rtrrtr', 'tettt', '0000-00-00', 'ttre', 'rrtr', 'à¦¬à§‡à¦¸à¦°à¦•à¦¾à¦°à§€ à¦šà¦¾à¦•à§à¦°à§€', 'ttrtrt', 'tttt', 'wewe', 'ewe', 'ewwer', 'ewew', 'ewrwe', 'wewe', 'ewwerw', 'ewrwrw', 'wew', 'ree', 'ffff', 'ererf', 'dfdfd', 'dfdf', 'fddf', 'dffds', '0000-00-00', 'dffdf', 'dffd', null, null, null, null, null, null, null, '2019-05-14 11:04:28', '2019-05-14 11:04:28');
INSERT INTO `members` VALUES ('8', 'à¦—à§à¦°à¦¾à¦¹à¦•à§‡à¦° à¦¨à¦¾à¦®', '', '', '0000-00-00', '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', null, null, null, null, null, null, null, '2019-05-28 08:07:32', '2019-05-28 08:07:32');
INSERT INTO `members` VALUES ('9', '', '', '', null, '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', null, null, null, null, null, null, null, '2019-05-28 09:16:36', '2019-05-28 09:16:36');
INSERT INTO `members` VALUES ('10', '', '', '', null, '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', '1559113273396.png', '1559113273457.png', '155911327365.png', '1559113273924.png', '##', '18', null, '2019-05-29 09:01:13', '2019-05-29 09:01:13');
INSERT INTO `members` VALUES ('11', '', '', '', null, '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', null, null, null, null, null, '19', '6', '2019-05-29 09:20:54', '2019-05-29 09:20:54');
INSERT INTO `members` VALUES ('12', 'ssds', 'wewer', '', null, '', '', '0', 'efe', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', null, null, null, null, null, '1', '1', '2019-06-12 07:36:39', '2019-06-12 07:36:39');
INSERT INTO `members` VALUES ('13', '', '', '', null, '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', null, null, null, null, null, '2', null, '2019-06-13 12:08:23', '2019-06-13 12:08:23');
INSERT INTO `members` VALUES ('14', null, '', '', null, '', '', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, '', '', null, null, null, null, null, '3', null, '2019-06-13 12:17:18', '2019-06-13 12:17:18');

-- ----------------------------
-- Table structure for `openbankaccounts`
-- ----------------------------
DROP TABLE IF EXISTS `openbankaccounts`;
CREATE TABLE `openbankaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bankName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accountNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mainBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of openbankaccounts
-- ----------------------------

-- ----------------------------
-- Table structure for `openloanaccounts`
-- ----------------------------
DROP TABLE IF EXISTS `openloanaccounts`;
CREATE TABLE `openloanaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `fatherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `age` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalidNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prePost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupassion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loanIdNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loanNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `loanAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit_10` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceCharge` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `totalAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installments` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstGuarantorId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `secondGuarantorId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientImage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientSignature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of openloanaccounts
-- ----------------------------

-- ----------------------------
-- Table structure for `openstaffaccounts`
-- ----------------------------
DROP TABLE IF EXISTS `openstaffaccounts`;
CREATE TABLE `openstaffaccounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accNo` text COLLATE utf8_unicode_ci,
  `accType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `accOpenDate` datetime DEFAULT NULL,
  `memberName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fatherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motherName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `husbandWifeName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationalIDNo` text COLLATE utf8_unicode_ci,
  `preAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prePost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preDistrisct` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `perDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` text COLLATE utf8_unicode_ci,
  `email` text COLLATE utf8_unicode_ci,
  `introducerAccNo` text COLLATE utf8_unicode_ci,
  `introducerMobile` text COLLATE utf8_unicode_ci,
  `introducerReputation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introducerName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `introducerAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomFather` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMother` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPreAddress` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPost` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomThana` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomDistrict` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomRelation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomBirthDate` date DEFAULT NULL,
  `percent` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomMobile` text COLLATE utf8_unicode_ci,
  `momberPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberSign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomPhoto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomSign` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of openstaffaccounts
-- ----------------------------

-- ----------------------------
-- Table structure for `payments`
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `income` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expencesCr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentBalanceDr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of payments
-- ----------------------------

-- ----------------------------
-- Table structure for `profitpayments`
-- ----------------------------
DROP TABLE IF EXISTS `profitpayments`;
CREATE TABLE `profitpayments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `branch` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `acctype` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `peymentPeriod` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `percentage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of profitpayments
-- ----------------------------

-- ----------------------------
-- Table structure for `staffs`
-- ----------------------------
DROP TABLE IF EXISTS `staffs`;
CREATE TABLE `staffs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nationalIDNo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bloodGrp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `father` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mother` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `joiningDate` datetime DEFAULT NULL,
  `moCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdmeCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dmeCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of staffs
-- ----------------------------
INSERT INTO `staffs` VALUES ('1', 'asdf', 'aaa', '123579', 'asdh@gmail.com', 'tyyy555', '445', '4444', 'ffhg', '4444.00', '2019-05-14 12:02:00', '3344', null, null, '1', null, '2019-05-14 12:03:32', '2019-05-14 12:03:32');
INSERT INTO `staffs` VALUES ('2', 'saasa', 'ssssassa', '1223', 'sdshgdf@gmail.com', 'czxbczn', 'cbhjc', 'sdjhc', 'chsdc', '3323.00', '2019-05-14 12:19:00', '', '2233', null, '1', null, '2019-05-14 12:20:34', '2019-05-14 12:20:34');
INSERT INTO `staffs` VALUES ('3', 'sda', 'sdas', '342', 'c', 'sd', 'sd', 'fdf', 'dsf', '433434.00', '2019-05-21 15:35:34', null, null, '23232', '2', null, '2019-05-21 15:35:43', '2019-05-21 15:35:46');

-- ----------------------------
-- Table structure for `transfers`
-- ----------------------------
DROP TABLE IF EXISTS `transfers`;
CREATE TABLE `transfers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expencesDr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `earningsCr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modified` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of transfers
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin@gmail.com', 'c9eba798d5dcbc0381d8cecd37be72c8fe615b31', '2019-05-20 12:33:30', '2019-05-20 12:33:33');

-- ----------------------------
-- Table structure for `withdrawpostings`
-- ----------------------------
DROP TABLE IF EXISTS `withdrawpostings`;
CREATE TABLE `withdrawpostings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accNo` text COLLATE utf8_unicode_ci,
  `paymentAmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sureAmmount` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payemntDate` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `memberName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paymentType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currentBalance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of withdrawpostings
-- ----------------------------
