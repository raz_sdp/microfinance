/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : microfinance

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2019-05-28 12:19:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `accounttypes`
-- ----------------------------
DROP TABLE IF EXISTS `accounttypes`;
CREATE TABLE `accounttypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accountType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of accounttypes
-- ----------------------------
INSERT INTO `accounttypes` VALUES ('1', 'gs', 'সাধারণ সঞ্চয়ী হিসাব', '2019-05-14 11:07:56', '2019-05-14 11:07:56');
INSERT INTO `accounttypes` VALUES ('2', 'ds', 'দৈনিক সঞ্চয়ী হিসাব', '2019-05-14 11:08:02', '2019-05-14 11:08:02');
INSERT INTO `accounttypes` VALUES ('3', 'ys', 'বাৎসরিক সঞ্চয়ী হিসাব', '2019-05-14 11:08:08', '2019-05-14 11:08:08');
INSERT INTO `accounttypes` VALUES ('4', 'psp', 'পি এস পি', '2019-05-14 11:08:19', '2019-05-14 11:08:19');
INSERT INTO `accounttypes` VALUES ('5', 'ms', 'মাসিক সঞ্চয়ী হিসাব', '2019-05-28 11:51:20', '2019-05-28 11:51:23');
INSERT INTO `accounttypes` VALUES ('6', 'ps', 'পি এস', '2019-05-28 11:52:13', '2019-05-28 11:52:19');
INSERT INTO `accounttypes` VALUES ('7', 'loan', 'লোন', '2019-05-28 11:54:44', '2019-05-28 11:54:46');
INSERT INTO `accounttypes` VALUES ('8', 'td', ' à¦¬à¦¿à¦œà¦¯à¦¼ - à¦‡à¦‰à¦¨à¦¿à¦•à§‹à¦¡ à¦•à¦¨à¦­à¦¾à¦°à§à¦Ÿà¦¾à¦°', '2019-05-28 08:16:13', '2019-05-28 08:16:13');
INSERT INTO `accounttypes` VALUES ('9', 'td', ' বিজয় - ইউনিকোড কনভার্টার', '2019-05-28 08:16:59', '2019-05-28 08:16:59');
